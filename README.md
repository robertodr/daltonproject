[![Documentation Status](https://readthedocs.org/projects/daltonproject/badge/?version=latest)](http://daltonproject.readthedocs.io/en/latest/?badge=latest)

# Dalton Project

The Dalton Project is a platform for quantum chemistry programs and libraries that are developed by the Dalton community.
Documentation can be found on [http://daltonproject.readthedocs.io/en/latest/](http://daltonproject.readthedocs.io/en/latest/)

## For developers

The code linting tools can be setup to work automatically as pre-commits by running the following from the root of the Dalton Project directory:

    $ pip install pre-commit
    $ pre-commit install

This will ensure that the linting and style of the project is ensured with every commit.

## Installation

Download

    $ git clone https://gitlab.com/DaltonProject/daltonproject.git

Create and activate a virtual environment (python version >= 3.6)

    $ virtualenv venv -p python3.6
    $ source venv/bin/activate
    $ pip install -r requirements.txt

For the above, you may need to first install virtualenv (requires Python 3)

    $ pip install virtualenv

Add to your PATH and run tests

    $ python -m pytest
    platform linux -- Python 3.7.6, pytest-5.3.2, py-1.8.1, pluggy-0.13.1
    rootdir: /home/user/Programs/daltonproject
    plugins: datadir-1.3.1, cov-2.8.1
    collected 137 items

    tests/test_basis.py ....                                                 [  2%]
    tests/test_dalton_ifc.py ......................................          [ 30%]
    tests/test_integration.py ...                                            [ 32%]
    tests/test_lsdalton_ifc.py ....                                          [ 35%]
    tests/test_mol_reader.py ...                                             [ 37%]
    tests/test_molecule.py ............                                      [ 46%]
    tests/test_natural_occupation_analyser.py ....                           [ 49%]
    tests/test_postprocess.py ....                                           [ 52%]
    tests/test_property.py ...........                                       [ 60%]
    tests/test_qcmethod.py ........                                          [ 66%]
    tests/test_qcprogram.py ...                                              [ 68%]
    tests/test_spectrum.py ...                                               [ 70%]
    tests/test_symmetry.py ........................................          [100%]

    ============================= 137 passed in 56.91s =============================



## Example

    >>> import daltonproject as dp
    >>> dalton = dp.QCProgram(program='Dalton')
    >>> dft = dp.QCMethod('DFT', 'B3LYP')
    >>> basis = dp.Basis(basis='pcseg-0')
    >>> ethanol = dp.Molecule(xyz='examples/data/ethanol.xyz', charge=0)
    >>> property = dp.Property(two_photon_absorption=True)
    >>> result = dalton.compute(dft, basis, ethanol, property)
    >>> output = dp.PostProcess(dalton, result)
    >>> for energy, cross_section in zip(output.excitation_energies, output.two_photon_cross_sections):
    ...     print(f"{energy:6.3f} {cross_section:6.3f}")
     7.177  0.143
     8.497  0.965
     8.926  0.293
     9.449  1.300
     9.620  0.727
