import numpy as np

from daltonproject import spectrum


def test_normalization():
    freq = np.linspace(0.0, 10.0, 20000)
    gaussian = spectrum.gaussian(freq, 5.0, 0.6)
    assert np.allclose(np.trapz(gaussian, freq), 1.0)


def test_tpa_convolution():
    excitation_energies = np.array([9.21080934, 12.0584537, 12.58553825, 13.41155621, 16.55956801])
    two_photon_strengths = np.array([1.18, 2.48, 6.39, 14.2, 14.9])
    two_photon_cross_sections = np.array([0.07456077, 0.29702821, 0.77662478, 1.88744448, 2.99770944])

    assert np.allclose(
        spectrum.convolute_two_photon_absorption(
            excitation_energies,
            two_photon_strengths,
            frequencies=excitation_energies / 2.0,
            broadening_function=spectrum.lorentzian,
            broadening_factor=0.1,
        ),
        two_photon_cross_sections,
    )


def test_opa_convolution():
    excitation_energies = np.array([4.3587, 6.4307, 8.0472])
    oscillator_strengths = np.array([0.0002, 0.5035, 0.0061])
    max_abs = np.max(
        spectrum.convolute_one_photon_absorption(excitation_energies,
                                                 oscillator_strengths,
                                                 np.linspace(2, 15, 300),
                                                 broadening_factor=0.325,
                                                 broadening_function=spectrum.gaussian))
    assert np.allclose(max_abs, 20900.925174314107)
    max_abs = np.max(
        spectrum.convolute_one_photon_absorption(excitation_energies,
                                                 oscillator_strengths,
                                                 np.linspace(7.4, 15, 300),
                                                 broadening_factor=0.325,
                                                 broadening_function=spectrum.gaussian))
    assert np.allclose(max_abs, 253.20571306999807)
