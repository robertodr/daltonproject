import os
from pathlib import Path

import pytest

import daltonproject as dp


def test_get_atom_basis():
    atom_basis = dp.lsdalton_ifc.get_atom_basis(basis='STO-3G', num_atoms=3, labels=['O1', 'H1', 'H2'])
    assert atom_basis == ['STO-3G', 'STO-3G', 'STO-3G']
    atom_basis = dp.lsdalton_ifc.get_atom_basis(
        basis={'H1': 'STO-3G', 'H2': 'STO-3G', 'O1': '6-31G*'},
        num_atoms=3,
        labels=['O1', 'H1', 'H2'],
    )
    assert atom_basis == ['6-31G*', 'STO-3G', 'STO-3G']


def test_get_atom_basis_exceptions():
    with pytest.raises(KeyError, match='No basis for O1 was provided.'):
        dp.lsdalton_ifc.get_atom_basis(basis={'H1': 'STO-3G', 'H2': 'STO-3G'}, num_atoms=3, labels=['O1', 'H1', 'H2'])


def test_no_rerun(shared_datadir):
    """Testing that a calculation will not rerun if the output file exist.
    """
    filename = '39f5b17878b109b89c7d247cf36a3d5948abe9d0'
    with Path(f'{filename}.out').open('w') as f:
        f.write('\n@    Final HF energy:              -2.855160477243\n')

    with Path(f'{filename}.tar.gz').open('w') as f:
        f.write('\n\n')

    with pytest.warns(UserWarning,
                      match=f'Files {filename}.out and {filename}.tar.gz already exist.' +
                      ' Not running the calculation again but will instead reuse old output files.'):
        lsdalton = dp.QCProgram(program='LSDalton')
        molecule = dp.Molecule(xyz=(shared_datadir / 'He.xyz'), charge=0)
        result = lsdalton.compute(dp.QCMethod('HF'), dp.Basis(basis='3-21G'), molecule, dp.Property(energy=True))
    output = dp.PostProcess(lsdalton, result)
    assert output.energy == pytest.approx(-2.855160477243)
    os.remove(f'{filename}.out')
    os.remove(f'{filename}.tar.gz')


def test_lsdalton_input():
    """Testing what will be written to the dal file
    """
    hf = dp.QCMethod('HF')
    prop = dp.Property(dipole_gradient=True)

    ref_lsdalton_input = ('**GENERAL\n' + '.NOGCBASIS\n' + '.TIME\n' + '**WAVE FUNCTION\n' + '.HF\n' + '*DENSOPT\n' +
                          '.VanLenthe\n' + '**INTEGRALS\n' + '.NOFAMILY\n' + '**OPENRSP\n' + '*RESPONSE FUNCTION\n' +
                          '$PROPERTY\n' + '.PERTURBATION\n' + '2\n' + 'GEO\n' + 'GEO\n' + '.KN-RULE\n' + '0\n' +
                          '$PROPERTY\n' + '.PERTURBATION\n' + '2\n' + 'GEO\n' + 'EL\n' + '.KN-RULE\n' + '0\n' +
                          '*END OF INPUT\n')

    lsdalton_input = dp.lsdalton_ifc.lsdalton_input(hf, prop)

    assert ref_lsdalton_input == lsdalton_input
    prop = dp.Property()
    prop.polarizability_gradient(frequencies=[0.0, 0.1])

    ref_lsdalton_input = ('**GENERAL\n' + '.NOGCBASIS\n' + '.TIME\n' + '**WAVE FUNCTION\n' + '.HF\n' + '*DENSOPT\n' +
                          '.VanLenthe\n' + '**INTEGRALS\n' + '.NOFAMILY\n' + '**OPENRSP\n' + '*RESPONSE FUNCTION\n' +
                          '$PROPERTY\n' + '.PERTURBATION\n' + '2\n' + 'GEO\n' + 'GEO\n' + '.KN-RULE\n' + '0\n' +
                          '$PROPERTY\n' + '.PERTURBATION\n' + '3\n' + 'GEO\n' + 'EL\n' + 'EL\n' + '.FREQUENCY\n' +
                          '2\n' + '0.0\n' + '0.1\n' + '.KN-RULE\n' + '0\n'
                          '*END OF INPUT\n')

    lsdalton_input = dp.lsdalton_ifc.lsdalton_input(hf, prop)

    assert ref_lsdalton_input == lsdalton_input
