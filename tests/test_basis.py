import pytest

import daltonproject as dp


def test_basis_str():
    basis = dp.Basis(basis='6-31G*')
    assert isinstance(basis.basis, str)
    assert basis.basis == '6-31G*'


def test_basis_dict():
    basis_dict = {'O': 'aug-pcseg-2', 'H': 'pcseg-2'}
    basis = dp.Basis(basis=basis_dict)
    assert isinstance(basis.basis, dict)
    for key, value in basis.basis.items():
        assert key in basis_dict


def test_basis_ri_admm():
    basis = dp.Basis(basis='6-31G*', ri='df-def2', admm='admm-1')
    assert isinstance(basis.basis, str)
    assert isinstance(basis.ri, str)
    assert isinstance(basis.admm, str)
    assert basis.basis == '6-31G*'
    assert basis.ri == 'df-def2'
    assert basis.admm == 'admm-1'


def test_basis_exception():
    with pytest.raises(TypeError, match=f'Unsupported basis set type: {list}'):
        dp.Basis(basis=['pcseg-2', 'pcseg-1'])
    with pytest.raises(TypeError, match=f'Unknown basis type: {list}'):
        dp.Basis(basis={'X': []})
    with pytest.raises(TypeError, match=f'Invalid atom label type: {int}'):
        dp.Basis(basis={1: 'pcseg-1'})
