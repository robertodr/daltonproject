import copy
import os
from typing import List

import numpy as np

import daltonproject as dp


def check_if_molecule_is_same(xyz_original: np.ndarray,
                              xyz_new: np.ndarray,
                              labels_original: List[str],
                              labels_new: List[str],
                              threshold: float = 10**-3) -> bool:
    """Checks if two molecules are identical within a threshold.

    Args:
      xyz_original: Molecule coordinates.
      xyz_new: Molecule coordinates.
      labels_original: Labels.
      labels_new: Labels.
      threshold: Threshold for molecules being identical.

    Returns:
      True, if the molecules are identical.
    """
    if len(xyz_original) != len(xyz_new):
        return False
    elif len(xyz_original) != len(labels_original):
        return False
    elif len(xyz_original) != len(labels_new):
        return False
    same_check = True
    atom_same_check = np.zeros(len(xyz_original))
    for i in range(len(xyz_original)):
        min_distance2 = 10**12
        for j in range(len(xyz_new)):
            if labels_original[i] != labels_new[j]:
                continue
            if atom_same_check[j] != 0:
                continue
            distance2 = np.sum((xyz_original[i] - xyz_new[j])**2)
            if distance2 < min_distance2:
                idx_closest = j
                min_distance2 = distance2
                if min_distance2**0.5 < threshold:
                    break
        if min_distance2**0.5 < threshold:
            atom_same_check[idx_closest] = 1
        else:
            same_check = False
            break
    return same_check


def test_multiple_writes(shared_datadir):
    """Test that the written files are identical if two files are written.
    Test made after a bug where the elements of the molecule would change when writing the mol-file.
    """
    mol = dp.Molecule(xyz=(shared_datadir / 'Hypochloric_Acid.xyz'), symmetry=True)
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp2')
    molecule2, basis2 = dp.mol_reader('tmp2.mol')
    assert check_if_molecule_is_same(molecule.coordinates, molecule2.coordinates, molecule.labels, molecule2.labels)
    os.remove('tmp.mol')
    os.remove('tmp2.mol')


def test_c2v_different_orientations(shared_datadir):
    """Test that different initial orientations of water leads to the same input.
    """
    mol = dp.Molecule(xyz=(shared_datadir / 'water_orientation1.xyz'), symmetry=True)
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'water_orientation1.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    mol = dp.Molecule(xyz=(shared_datadir / 'water_orientation2.xyz'), symmetry=True)
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'water_orientation1.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    mol = dp.Molecule(xyz=(shared_datadir / 'water_orientation3.xyz'), symmetry=True)
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'water_orientation1.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_c1_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'not_methanol.xyz'), symmetry=True)
    assert mol.point_group == 'C1'


def test_k_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'He.xyz'), symmetry=True)
    assert mol.point_group == 'K'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'He.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_cs_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Hypochloric_Acid.xyz'), symmetry=True)
    assert mol.point_group == 'Cs'
    mol = dp.Molecule(xyz=(shared_datadir / 'S7-Sulfur.xyz'), symmetry=True)
    assert mol.point_group == 'Cs'
    mol = dp.Molecule(xyz=(shared_datadir / 'Quinoline.xyz'), symmetry=True)
    assert mol.point_group == 'Cs'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Quinoline.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_ci_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'meso-tartaric_acid.xyz'), symmetry=True)
    assert mol.point_group == 'Ci'
    mol = dp.Molecule(xyz=(shared_datadir / 'staggered-1,2-Dibromo-1,2-dichloroethane.xyz'), symmetry=True)
    assert mol.point_group == 'Ci'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'staggered-1,2-Dibromo-1,2-dichloroethane.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_c2_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'hydrogenperoxide.xyz'), symmetry=True)
    assert mol.point_group == 'C2'
    mol = dp.Molecule(xyz=(shared_datadir / '1,3-dichloroallene.xyz'), symmetry=True)
    assert mol.point_group == 'C2'
    mol = dp.Molecule(xyz=(shared_datadir / 'half-chair-cyclopentane.xyz'), symmetry=True)
    assert mol.point_group == 'C2'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'half-chair-cyclopentane.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_c3_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'rotated-1,1,1-Trichloroethane.xyz'), symmetry=True)
    assert mol.point_group == 'C3'
    mol = dp.Molecule(xyz=(shared_datadir / 'Triphenylmethane.xyz'), symmetry=True)
    assert mol.point_group == 'C3'


def test_d2_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Biphenyl.xyz'), symmetry=True)
    assert mol.point_group == 'D2'
    mol = dp.Molecule(xyz=(shared_datadir / 'Twistane.xyz'), symmetry=True)
    assert mol.point_group == 'D2'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Twistane.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_d3_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'rotated-ethane.xyz'), symmetry=True)
    assert mol.point_group == 'D3'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    with open('tmp.mol') as f:
        for line in f:
            print(line, end='')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'rotated-ethane.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_d5_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'rotated-ferrocene.xyz'), symmetry=True)
    assert mol.point_group == 'D5'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'rotated-ferrocene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_d6_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'rotated-Bis(benzene)chromium.xyz'), symmetry=True)
    assert mol.point_group == 'D6'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'rotated-Bis(benzene)chromium.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_d8_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'rotated-uranocene.xyz'), symmetry=True)
    assert mol.point_group == 'D8'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'rotated-uranocene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_c2v_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'water.xyz'), symmetry=True)
    assert mol.point_group == 'C2v'
    mol = dp.Molecule(xyz=(shared_datadir / 'Squaric_Acid_Difluoride.xyz'), symmetry=True)
    assert mol.point_group == 'C2v'
    mol = dp.Molecule(xyz=(shared_datadir / 'Phenanthrene.xyz'), symmetry=True)
    assert mol.point_group == 'C2v'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Phenanthrene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_c3v_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Ammonia.xyz'), symmetry=True)
    assert mol.point_group == 'C3v'
    mol = dp.Molecule(xyz=(shared_datadir / '1-Azabicyclo[2.2.2]octane.xyz'), symmetry=True)
    assert mol.point_group == 'C3v'
    mol = dp.Molecule(xyz=(shared_datadir / 'Sumanene.xyz'), symmetry=True)
    assert mol.point_group == 'C3v'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Sumanene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_c4v_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Brominepentafluoride.xyz'), symmetry=True)
    assert mol.point_group == 'C4v'
    mol = dp.Molecule(xyz=(shared_datadir / 'Sulfurchloridepentafluoride.xyz'), symmetry=True)
    assert mol.point_group == 'C4v'
    mol = dp.Molecule(xyz=(shared_datadir / '9-Pentaborane.xyz'), symmetry=True)
    assert mol.point_group == 'C4v'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / '9-Pentaborane.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_c5v_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Corannulene.xyz'), symmetry=True)
    assert mol.point_group == 'C5v'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Corannulene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_c2h_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'trans-1,2-Dichloroethylene.xyz'), symmetry=True)
    assert mol.point_group == 'C2h'
    mol = dp.Molecule(xyz=(shared_datadir / 's-trans-Butadiene.xyz'), symmetry=True)
    assert mol.point_group == 'C2h'
    mol = dp.Molecule(xyz=(shared_datadir / 'Chrysene.xyz'), symmetry=True)
    assert mol.point_group == 'C2h'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Chrysene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_c3h_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Boric_Acid.xyz'), symmetry=True)
    assert mol.point_group == 'C3h'
    mol = dp.Molecule(xyz=(shared_datadir / 'Tricyclo[3.3.3.0(1,5)]undecane.xyz'), symmetry=True)
    assert mol.point_group == 'C3h'
    mol = dp.Molecule(xyz=(shared_datadir / 'Bicyclo[3.3.3]undecane.xyz'), symmetry=True)
    assert mol.point_group == 'C3h'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Bicyclo[3.3.3]undecane.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_d2h_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Ethylene.xyz'), symmetry=True)
    assert mol.point_group == 'D2h'
    mol = dp.Molecule(xyz=(shared_datadir / 'Tricyclo[3.1.1.1(2,4)]octane.xyz'), symmetry=True)
    assert mol.point_group == 'D2h'
    mol = dp.Molecule(xyz=(shared_datadir / 'Dianthracene.xyz'), symmetry=True)
    assert mol.point_group == 'D2h'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Dianthracene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_d3h_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Borontrifluoride.xyz'), symmetry=True)
    assert mol.point_group == 'D3h'
    mol = dp.Molecule(xyz=(shared_datadir / 'Ironpentacarbonyl.xyz'), symmetry=True)
    assert mol.point_group == 'D3h'
    mol = dp.Molecule(xyz=(shared_datadir / 'Triptycene.xyz'), symmetry=True)
    assert mol.point_group == 'D3h'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Triptycene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_d4h_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / '[Al4]2--Ion.xyz'), symmetry=True)
    assert mol.point_group == 'D4h'
    mol = dp.Molecule(xyz=(shared_datadir / 'Xenontetrafluoride.xyz'), symmetry=True)
    assert mol.point_group == 'D4h'
    mol = dp.Molecule(xyz=(shared_datadir / 'Squaric_Acid_Dianion.xyz'), symmetry=True)
    assert mol.point_group == 'D4h'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Squaric_Acid_Dianion.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_d5h_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Iodineheptafluoride.xyz'), symmetry=True)
    assert mol.point_group == 'D5h'
    mol = dp.Molecule(xyz=(shared_datadir / 'eclipsed-Ferrocene.xyz'), symmetry=True)
    assert mol.point_group == 'D5h'
    mol = dp.Molecule(xyz=(shared_datadir / 'C70-Fullerene.xyz'), symmetry=True)
    assert mol.point_group == 'D5h'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'C70-Fullerene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_d6h_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Benzene.xyz'), symmetry=True)
    assert mol.point_group == 'D6h'
    mol = dp.Molecule(xyz=(shared_datadir / 'Superphane.xyz'), symmetry=True)
    assert mol.point_group == 'D6h'
    mol = dp.Molecule(xyz=(shared_datadir / 'Cucurbit[6]uril.xyz'), symmetry=True)
    assert mol.point_group == 'D6h'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Cucurbit[6]uril.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_d7h_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Tropylium_Cation.xyz'), symmetry=True)
    assert mol.point_group == 'D7h'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Tropylium_Cation.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_d8h_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Cyclooctatetraene_Dianion.xyz'), symmetry=True)
    assert mol.point_group == 'D8h'
    mol = dp.Molecule(xyz=(shared_datadir / 'Sulflower.xyz'), symmetry=True)
    assert mol.point_group == 'D8h'
    mol = dp.Molecule(xyz=(shared_datadir / 'eclipsed-Uranocene.xyz'), symmetry=True)
    assert mol.point_group == 'D8h'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'eclipsed-Uranocene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_d2d_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Allene.xyz'), symmetry=True)
    assert mol.point_group == 'D2d'
    mol = dp.Molecule(xyz=(shared_datadir / 'Cyclobutane.xyz'), symmetry=True)
    assert mol.point_group == 'D2d'
    mol = dp.Molecule(xyz=(shared_datadir / 'Cyclooctatetraene.xyz'), symmetry=True)
    assert mol.point_group == 'D2d'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Cyclooctatetraene.mol')
    ref2_coordinates = copy.deepcopy(ref_molecule.coordinates)
    ref2_coordinates[:, 2] = -ref2_coordinates[:, 2]
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels) or check_if_molecule_is_same(
                                         molecule.coordinates, ref2_coordinates, molecule.labels, ref_molecule.labels)
    os.remove('tmp.mol')


def test_d3d_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'S6-Sulfur.xyz'), symmetry=True)
    assert mol.point_group == 'D3d'
    mol = dp.Molecule(xyz=(shared_datadir / 'chair-Cyclohexane.xyz'), symmetry=True)
    assert mol.point_group == 'D3d'
    mol = dp.Molecule(xyz=(shared_datadir / 'Hexamethylbenzene.xyz'), symmetry=True)
    assert mol.point_group == 'D3d'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Hexamethylbenzene.mol')
    ref2_coordinates = copy.deepcopy(ref_molecule.coordinates)
    ref2_coordinates[:, 2] = -ref2_coordinates[:, 2]
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels) or check_if_molecule_is_same(
                                         molecule.coordinates, ref2_coordinates, molecule.labels, ref_molecule.labels)
    os.remove('tmp.mol')


def test_d4d_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'S8-Sulfur.xyz'), symmetry=True)
    assert mol.point_group == 'D4d'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'S8-Sulfur.mol')
    ref2_coordinates = copy.deepcopy(ref_molecule.coordinates)
    ref2_coordinates[:, 2] = -ref2_coordinates[:, 2]
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels) or check_if_molecule_is_same(
                                         molecule.coordinates, ref2_coordinates, molecule.labels, ref_molecule.labels)
    os.remove('tmp.mol')


def test_d5d_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'staggered-Ferrocene.xyz'), symmetry=True)
    assert mol.point_group == 'D5d'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'staggered-Ferrocene.mol')
    ref2_coordinates = copy.deepcopy(ref_molecule.coordinates)
    ref2_coordinates[:, 2] = -ref2_coordinates[:, 2]
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels) or check_if_molecule_is_same(
                                         molecule.coordinates, ref2_coordinates, molecule.labels, ref_molecule.labels)
    os.remove('tmp.mol')


def test_d6d_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'staggered-Bis(benzene)chromium.xyz'), symmetry=True)
    assert mol.point_group == 'D6d'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'staggered-Bis(benzene)chromium.mol')
    ref2_coordinates = copy.deepcopy(ref_molecule.coordinates)
    ref2_coordinates[:, 2] = -ref2_coordinates[:, 2]
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels) or check_if_molecule_is_same(
                                         molecule.coordinates, ref2_coordinates, molecule.labels, ref_molecule.labels)
    os.remove('tmp.mol')


def test_d8d_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'staggered-Uranocene.xyz'), symmetry=True)
    assert mol.point_group == 'D8d'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'staggered-Uranocene.mol')
    ref2_coordinates = copy.deepcopy(ref_molecule.coordinates)
    ref2_coordinates[:, 2] = -ref2_coordinates[:, 2]
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels) or check_if_molecule_is_same(
                                         molecule.coordinates, ref2_coordinates, molecule.labels, ref_molecule.labels)
    os.remove('tmp.mol')


def test_s4_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / '1,3,5,7-Tetrachlorocyclooctatetraene.xyz'), symmetry=True)
    assert mol.point_group == 'S4'
    mol = dp.Molecule(xyz=(shared_datadir / 'Tetrachloroneopentane.xyz'), symmetry=True)
    assert mol.point_group == 'S4'
    mol = dp.Molecule(xyz=(shared_datadir / 'Tetraphenylmethane.xyz'), symmetry=True)
    assert mol.point_group == 'S4'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Tetraphenylmethane.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_t_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'rotated-neopentane.xyz'), symmetry=True)
    assert mol.point_group == 'T'
    mol = dp.Molecule(xyz=(shared_datadir / 'C60F36-Fullerene.xyz'), symmetry=True)
    assert mol.point_group == 'T'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'C60F36-Fullerene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_th_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / '[Mg(H2O)6]2+-Ion.xyz'), symmetry=True)
    assert mol.point_group == 'Th'
    mol = dp.Molecule(xyz=(shared_datadir / 'C60Br24-Bromofullerene.xyz'), symmetry=True)
    assert mol.point_group == 'Th'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'C60Br24-Bromofullerene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_td_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Phosphorus.xyz'), symmetry=True)
    assert mol.point_group == 'Td'
    mol = dp.Molecule(xyz=(shared_datadir / 'Phosphoruspentoxide.xyz'), symmetry=True)
    assert mol.point_group == 'Td'
    mol = dp.Molecule(xyz=(shared_datadir / 'Tetrairidiumdodecacarbonyl.xyz'), symmetry=True)
    assert mol.point_group == 'Td'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Tetrairidiumdodecacarbonyl.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_o_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'rotated-Octamethylsilsesquioxane.xyz'), symmetry=True)
    assert mol.point_group == 'O'
    mol = dp.Molecule(xyz=(shared_datadir / 'Dodecaethyleneoctamine.xyz'), symmetry=True)
    assert mol.point_group == 'O'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Dodecaethyleneoctamine.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_oh_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Sulfurhexafluoride.xyz'), symmetry=True)
    assert mol.point_group == 'Oh'
    mol = dp.Molecule(xyz=(shared_datadir / 'Tungstenhexacarbonyl.xyz'), symmetry=True)
    assert mol.point_group == 'Oh'
    mol = dp.Molecule(xyz=(shared_datadir / 'Octamethylsilsesquioxane.xyz'), symmetry=True)
    assert mol.point_group == 'Oh'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Octamethylsilsesquioxane.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_ih_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'C60-Fullerene.xyz'), symmetry=True)
    assert mol.point_group == 'Ih'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'C60-Fullerene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_coov_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Hydrogen_Chloride.xyz'), symmetry=True)
    assert mol.point_group == 'Coov'
    mol = dp.Molecule(xyz=(shared_datadir / 'Fluoroacetylene.xyz'), symmetry=True)
    assert mol.point_group == 'Coov'
    mol = dp.Molecule(xyz=(shared_datadir / 'Fluorodiacetylene.xyz'), symmetry=True)
    assert mol.point_group == 'Coov'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Fluorodiacetylene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')


def test_dooh_point_group(shared_datadir):
    mol = dp.Molecule(xyz=(shared_datadir / 'Hydrogen.xyz'), symmetry=True)
    assert mol.point_group == 'Dooh'
    mol = dp.Molecule(xyz=(shared_datadir / 'Acetylene.xyz'), symmetry=True)
    assert mol.point_group == 'Dooh'
    mol = dp.Molecule(xyz=(shared_datadir / 'Diacetylene.xyz'), symmetry=True)
    assert mol.point_group == 'Dooh'
    dp.dalton_ifc.write_molecule_input(mol, dp.Basis('3-21G'), 'tmp')
    molecule, basis = dp.mol_reader('tmp.mol')
    ref_molecule, ref_basis = dp.mol_reader(shared_datadir / 'Diacetylene.mol')
    assert check_if_molecule_is_same(molecule.coordinates, ref_molecule.coordinates, molecule.labels,
                                     ref_molecule.labels)
    os.remove('tmp.mol')
