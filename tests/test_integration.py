import os

import numpy as np
import pytest

import daltonproject as dp


def test_tpa(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='pcseg-0')
    energy = dp.Property(energy=True, two_photon_absorption=True)
    water = dp.Molecule(xyz=(shared_datadir / 'water.xyz'), charge=0)
    result_1 = dalton.compute(hf, basis, water, energy)
    output = dp.PostProcess(dalton, result_1)
    assert np.allclose(-75.702096519278, output.energy)
    assert np.allclose([9.21080934, 12.0584537, 12.58553825, 13.41155621, 16.55956801], output.excitation_energies)
    assert np.allclose([1.18, 2.48, 6.39, 14.2, 14.9], output.two_photon_strengths)
    assert np.allclose([0.0732, 0.264, 0.741, 1.87, 2.99], output.two_photon_cross_sections)

    excita = dp.Property(excitation_energy=True)
    result_2 = dalton.compute(hf, basis, water, excita)
    output_excita = dp.PostProcess(dalton, result_2)
    assert np.allclose(output.excitation_energies, output_excita.excitation_energies)
    os.remove(f'{result_1}.out')
    os.remove(f'{result_1}.tar.gz')
    os.remove(f'{result_2}.out')
    os.remove(f'{result_2}.tar.gz')


def test_lsdalton_geo_dfJ_admm(shared_datadir):
    lsdalton = dp.QCProgram(program='LSDalton')
    b3lyp = dp.QCMethod('DFT', 'B3LYP')
    basis = dp.Basis(basis='pcseg-0', ri='df-def2', admm='admm-1')

    # Geometry optimization
    geo = dp.Property(geometry_optimization=True)
    water = dp.Molecule(xyz=(shared_datadir / 'water.xyz'), charge=0)
    result = lsdalton.compute(b3lyp, basis, water, geo)
    output = dp.PostProcess(lsdalton, result)
    assert np.allclose(-76.143249869196, output.energy)
    assert np.allclose(
        [
            [1.00963577e-01, -0.00000000e00, 6.40000000e-15],
            [7.08120212e-01, -0.00000000e00, 7.95314526e-01],
            [7.08120212e-01, -0.00000000e00, -7.95314526e-01],
        ],
        output.final_geometry,
    )
    water.coordinates = output.final_geometry
    os.remove(f'{result}.out')
    os.remove(f'{result}.tar.gz')

    # Excitation energies
    exci = dp.Property(excitation_energy=True)
    ex_res = lsdalton.compute(b3lyp, basis, water, exci)
    ex_out = dp.PostProcess(lsdalton, ex_res)
    assert np.allclose([6.93845695, 9.14764001, 9.18248964, 11.51374869, 13.67039309], ex_out.excitation_energies)
    assert np.allclose([0.00557589, 0.00000000, 0.08327072, 0.08157904, 0.38883407], ex_out.oscillator_strengths)
    os.remove(f'{ex_res}.out')
    os.remove(f'{ex_res}.tar.gz')


def test_hfc():
    dalton = dp.QCProgram(program='Dalton')
    dft = dp.QCMethod('DFT', 'BP86')
    mol = dp.Molecule(atoms="""\
Ti    0.000000  0.000000   0.000000
F     1.756834  0.000000   0.000000
F    -0.878417  1.521463   0.000000
F    -0.878417 -1.521463   0.000000""")
    basis = dp.Basis(basis='STO-3G')
    prop = dp.Property(energy=True, hfc_atoms=[1, 2])
    result = dalton.compute(dft, basis, mol, prop)
    output = dp.PostProcess(dalton, result)
    assert output.energy == pytest.approx(-1136.250771376731)
    assert np.allclose(output.hyperfine_coupling['total'], [15.337085, 0.312577])
    assert np.allclose(output.hyperfine_coupling['direct'], [14.941623, 0.318802])
    assert np.allclose(output.hyperfine_coupling['polarization'], [0.395462, -0.006226])
