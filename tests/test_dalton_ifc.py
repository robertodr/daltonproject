import os
import textwrap

import numpy as np
import pytest

import daltonproject as dp
import daltonproject.dalton_ifc as dalton_ifc


def test_get_atom_basis():
    atom_basis = dp.dalton_ifc.get_atom_basis(basis='STO-3G', num_atoms=3, labels=['O1', 'H1', 'H2'])
    assert atom_basis == ['STO-3G', 'STO-3G', 'STO-3G']
    atom_basis = dp.dalton_ifc.get_atom_basis(
        basis={'H1': 'STO-3G', 'H2': 'STO-3G', 'O1': '6-31G*'},
        num_atoms=3,
        labels=['O1', 'H1', 'H2'],
    )
    assert atom_basis == ['6-31G*', 'STO-3G', 'STO-3G']


def test_get_atom_basis_exceptions():
    with pytest.raises(KeyError, match='No basis for O1 was provided.'):
        dalton_ifc.get_atom_basis(basis={'H1': 'STO-3G', 'H2': 'STO-3G'}, num_atoms=3, labels=['O1', 'H1', 'H2'])


def test_dalton_ifc_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='3-21G')
    energy = dp.Property(energy=True)
    water = dp.Molecule(xyz=(shared_datadir / 'water.xyz'), charge=0)
    with pytest.raises(TypeError):
        dalton.compute(basis, basis, water, energy)
    with pytest.raises(TypeError):
        dalton.compute(hf, hf, water, energy)
    with pytest.raises(TypeError):
        dalton.compute(hf, basis, hf, energy)
    with pytest.raises(TypeError):
        dalton.compute(hf, basis, water, hf)


def test_get_orbital_coefficients(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, (shared_datadir / 'input_He_nosym'))
    reference = np.array([[1.00000010e00, -1.93982765e00, 0.00000000e00, 0.00000000e00, 0.00000000e00],
                          [-1.15851440e-07, 2.18241415e00, 0.00000000e00, 0.00000000e00, 0.00000000e00],
                          [0.00000000e00, 0.00000000e00, 0.00000000e00, 0.00000000e00, 1.00000000e00],
                          [0.00000000e00, 0.00000000e00, 1.00000000e00, 0.00000000e00, 0.00000000e00],
                          [0.00000000e00, 0.00000000e00, 0.00000000e00, 1.00000000e00, 0.00000000e00]])
    coeff = output.orbital_coefficients
    assert np.all(abs(reference - coeff)) < 1e-7
    output = dp.PostProcess(dalton, (shared_datadir / 'input_He'))
    reference = {
        1: np.array([[1.00000010e00, -1.93982765e00], [-1.16153510e-07, 2.18241415e00]]), 2: np.array([[1.0]]), 3:
        np.array([[1.0]]), 5: np.array([[1.0]])
    }
    coeff = output.orbital_coefficients
    assert np.all(abs(reference[1] - coeff[1])) < 1e-7
    assert np.all(abs(reference[2] - coeff[2])) < 1e-7
    assert np.all(abs(reference[3] - coeff[3])) < 1e-7
    assert np.all(abs(reference[5] - coeff[5])) < 1e-7


def test_get_num_orbitals(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, (shared_datadir / 'input_He_nosym'))
    orbitals = output.num_orbitals
    orbitals_reference = 5
    assert orbitals == orbitals_reference
    output = dp.PostProcess(dalton, (shared_datadir / 'input_He'))
    orbitals = output.num_orbitals
    orbitals_reference = [2, 1, 1, 0, 1, 0, 0, 0]
    assert orbitals == orbitals_reference


def test_get_num_basis_functions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, (shared_datadir / 'input_He_nosym'))
    bf = output.num_basis_functions
    bf_reference = 5
    assert bf == bf_reference
    output = dp.PostProcess(dalton, (shared_datadir / 'input_He'))
    bf = output.num_basis_functions
    bf_reference = [2, 1, 1, 0, 1, 0, 0, 0]
    assert bf == bf_reference


def test_hfsrdft_input(shared_datadir):
    with open((shared_datadir / 'hfsrdft.dal'), 'r') as file:
        reference = file.read()
    qcmethod = dp.QCMethod('HFSRDFT', 'SRXPBEGWS SRCPBEGWS')
    qcmethod.range_separation_parameter(0.4)
    input_file = dp.dalton_ifc.dalton_input(qcmethod, dp.Property(energy=True))
    assert input_file == reference


def test_mp2srdft_input(shared_datadir):
    with open((shared_datadir / 'mp2srdft.dal'), 'r') as file:
        reference = file.read()
    qcmethod = dp.QCMethod('MP2SRDFT', 'SRXPBEGWS SRCPBEGWS')
    qcmethod.range_separation_parameter(0.4)
    input_file = dp.dalton_ifc.dalton_input(qcmethod, dp.Property(energy=True))
    assert input_file == reference


def test_no_rerun(shared_datadir):
    """Testing that a calculation will not rerun if the output file exist.
    """
    filename = 'b5eada478eddacc04482ae7c4d586d7c798afbed'
    file = open(f'{filename}.out', 'w')
    file.write('\n@    Final HF energy:              -2.855160477243\n')
    file.close()
    file = open(f'{filename}.tar.gz', 'w')
    file.write('\n\n')
    file.close()
    with pytest.warns(UserWarning,
                      match=f'Files {filename}.out and {filename}.tar.gz already exist.' +
                      ' Not running the calculation again but will instead reuse old output files.'):
        dalton = dp.QCProgram(program='Dalton')
        molecule = dp.Molecule(xyz=(shared_datadir / 'He.xyz'), charge=0)
        result = dalton.compute(dp.QCMethod('HF'), dp.Basis(basis='3-21G'), molecule, dp.Property(energy=True))
    output = dp.PostProcess(dalton, result)
    assert abs(output.energy - (-2.855160477243)) < 1e-11
    os.remove(f'{filename}.out')
    os.remove(f'{filename}.tar.gz')


def test_get_natural_occupations(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, (shared_datadir / 'mp2_He'))
    nat_occ = output.natural_occupations
    nat_occ_reference = {
        1: [1.99090161, 0.00484282], 2: [0.00141853], 3: [0.00141853], 4: [], 5: [0.00141853], 6: [], 7: [], 8: []
    }
    for key in nat_occ_reference:
        assert np.all(nat_occ[key] == nat_occ_reference[key])
    output = dp.PostProcess(dalton, (shared_datadir / 'mp2_He_nosym'))
    nat_occ = output.natural_occupations
    nat_occ_reference = {1: [1.99090161, 0.00484282, 0.00141853, 0.00141853, 0.00141853]}
    for key in nat_occ_reference:
        assert np.all(nat_occ[key] == nat_occ_reference[key])
    output = dp.PostProcess(dalton, (shared_datadir / 'ci_He'))
    nat_occ = output.natural_occupations
    nat_occ_reference = {
        1: [1.985492101, 0.008323780], 2: [0.002061373], 3: [0.002061373], 4: [], 5: [0.002061373], 6: [], 7: [], 8:
        []
    }
    for key in nat_occ_reference:
        assert np.all(nat_occ[key] == nat_occ_reference[key])
    output = dp.PostProcess(dalton, (shared_datadir / 'ci_He_nosym'))
    nat_occ = output.natural_occupations
    nat_occ_reference = {1: [1.985492101, 0.008323780, 0.002061373, 0.002061373, 0.002061373]}
    for key in nat_occ_reference:
        assert np.all(nat_occ[key] == nat_occ_reference[key])


def test_casscf_input(shared_datadir):
    with open((shared_datadir / 'casscf.dal'), 'r') as file:
        reference = file.read()
    dalton = dp.QCProgram(program='Dalton')
    # Can just use the output from an mp2srdft calculation for this test.
    mp2_output = dp.PostProcess(dalton, (shared_datadir / 'mp2srdft_Be'))
    electrons, cas, inactive = mp2_output.pick_cas_by_thresholds(1.97, 0.01)
    casscf = dp.QCMethod('CASSCF')
    casscf.complete_active_space(electrons, cas, inactive)
    casscf.input_orbital_coefficients(mp2_output.orbital_coefficients)
    input_file = dp.dalton_ifc.dalton_input(casscf, dp.Property(energy=True))
    assert input_file == reference


def test_casscf_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    mp2_output = dp.PostProcess(dalton, (shared_datadir / 'mp2srdft_Be'))
    electrons, cas, inactive = mp2_output.pick_cas_by_thresholds(1.97, 0.01)
    casscf = dp.QCMethod('CASSCF')
    casscf.complete_active_space(electrons, cas, inactive)
    with pytest.raises(AttributeError, match='No orbitals were supplied for the CAS calculation.'):
        dp.dalton_ifc.dalton_input(casscf, dp.Property(energy=True))


def test_cassrdft_input(shared_datadir):
    with open((shared_datadir / 'cassrdft.dal'), 'r') as file:
        reference = file.read()
    dalton = dp.QCProgram(program='Dalton')
    # Can just use the output from an mp2srdft calculation for this test.
    mp2_output = dp.PostProcess(dalton, (shared_datadir / 'mp2srdft_Be'))
    electrons, cas, inactive = mp2_output.pick_cas_by_thresholds(1.97, 0.01)
    cassrdft = dp.QCMethod('CASsrDFT', 'SRXPBEGWS SRCPBEGWS')
    cassrdft.range_separation_parameter(0.4)
    cassrdft.complete_active_space(electrons, cas, inactive)
    cassrdft.input_orbital_coefficients(mp2_output.orbital_coefficients)
    input_file = dp.dalton_ifc.dalton_input(cassrdft, dp.Property(energy=True))
    assert input_file == reference


def test_cassrdft_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    mp2_output = dp.PostProcess(dalton, (shared_datadir / 'mp2srdft_Be'))
    electrons, cas, inactive = mp2_output.pick_cas_by_thresholds(1.97, 0.01)
    cassrdft = dp.QCMethod('CASsrDFT', 'SRXPBEGWS SRCPBEGWS')
    cassrdft.range_separation_parameter(0.4)
    cassrdft.complete_active_space(electrons, cas, inactive)
    with pytest.raises(AttributeError, match='No orbitals were supplied for the CAS calculation.'):
        dp.dalton_ifc.dalton_input(cassrdft, dp.Property(energy=True))


def test_punch_orbitals(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, (shared_datadir / 'deleteorbitals_Ne'))
    reference = (
        '  0.98193507302637 -0.11438139672414  0.08851269889772  0.00000000000000\n  0.00000000000000\n -0.06932190849722  0.52375771268112  0.49857107704165  0.00000000000000\n  0.00000000000000\n -0.00000000000000 -0.00000000000000 -0.00000000000000  1.00000000000000\n  0.00000000000000\n  0.00000000000000  0.00000000000000  0.00000000000000  0.00000000000000\n  1.00000000000000\n  0.00000000000000  0.00000000000000  0.00000000000000  0.00000000000000\n  0.00000000000000\n  0.52809446965387  0.52809446965387\n  0.00000000000000  0.00000000000000\n  0.52809446965387  0.52809446965387\n  0.00000000000000  0.00000000000000\n  1.00000000000000\n  0.52809446965387  0.52809446965387\n  0.00000000000000  0.00000000000000\n  1.00000000000000\n  1.00000000000000',
        [1, 1, 1, 0, 1, 0, 0, 0])
    assert dp.dalton_ifc.punch_orbitals(output.orbital_coefficients) == reference
    output = dp.PostProcess(dalton, (shared_datadir / 'big_mo_coeffs'))
    reference = ('  0.01346601772099  0.06798611094381  0.11127926896129 -0.09365627542679\n -0.00940795794576\n'
                 '  13.4660177209900  67.9861109438100 111.2792689612900 -93.6562754267900\n  -9.4079579457600\n'
                 '  134.660177209900  679.861109438100 1112.792689612900 -936.562754267900\n  -94.079579457600\n'
                 '  1346.60177209900  6798.61109438100 11127.92689612900 -9365.62754267900\n  -940.79579457600\n'
                 '  13466.0177209900  67986.1109438100 111279.2689612900 -93656.2754267900\n  -9407.9579457600', [0])
    assert dp.dalton_ifc.punch_orbitals(output.orbital_coefficients) == reference


def test_punch_orbitals_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    # Can just use the output from an mp2srdft calculation for this test.
    mp2_output = dp.PostProcess(dalton, (shared_datadir / 'mp2srdft_Be'))
    with pytest.raises(ValueError, match='Orbital coefficient to large, orbitals from dal file will be broken.'):
        dp.dalton_ifc.punch_orbitals(mp2_output.orbital_coefficients * 10**6)


def test_casscf_deleted_orbitals(shared_datadir):
    with open((shared_datadir / 'casscf_deletedorbitals.dal'), 'r') as file:
        reference = file.read()
    dalton = dp.QCProgram(program='Dalton')
    # Can just use the output from an mp2srdft calculation for this test.
    mp2_output = dp.PostProcess(dalton, (shared_datadir / 'deleteorbitals_Ne'))
    electrons, cas, inactive = mp2_output.pick_cas_by_thresholds(1.97, 0.01)
    casscf = dp.QCMethod('CASSCF')
    casscf.complete_active_space(electrons, cas, inactive)
    casscf.input_orbital_coefficients(mp2_output.orbital_coefficients)
    input_file = dp.dalton_ifc.dalton_input(casscf, dp.Property(energy=True))
    assert input_file == reference


def test_energy_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    filename = (shared_datadir / 'empty_file')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(Exception, match=f'Energy not found in output: {filename}'):
        output.energy


def test_electronic_energy(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, (shared_datadir / 'input_He_nosym'))
    reference = -2.855160477243
    assert np.abs(output.electronic_energy - reference) < 1e-11


def test_electronic_energy_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    filename = (shared_datadir / 'empty_file')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(Exception, match=f'Electronic energy not found in output: {filename}'):
        output.electronic_energy


def test_nuclear_repulsion_energy(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, (shared_datadir / 'input_He_nosym'))
    reference = 0.000000000000
    assert np.abs(output.nuclear_repulsion_energy - reference) < 1e-11


def test_nuclear_repulsion_energy_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    filename = (shared_datadir / 'empty_file')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(Exception, match=f'Nuclear repulsion energy not found in output: {filename}'):
        output.nuclear_repulsion_energy


def test_num_electrons(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, (shared_datadir / 'input_He_nosym'))
    assert output.num_electrons == 2


def test_num_electrons_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    filename = (shared_datadir / 'empty_file')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(Exception, match=f'Number of electrons not found in output: {filename}'):
        output.num_electrons


def test_num_orbitals_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    filename = (shared_datadir / 'empty_file')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(Exception, match=f'Number of orbitals not found in output: {filename}'):
        output.num_orbitals


def test_num_basis_functions_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    filename = (shared_datadir / 'empty_file')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(Exception, match=f'Number of basis functions not found in output: {filename}'):
        output.num_basis_functions


def test_excitation_energies_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    filename = (shared_datadir / 'empty_file')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(NotImplementedError):
        output.excitation_energies
    filename = (shared_datadir / 'trigger_exceptions')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(Exception, match=f'Could not find excitation energies in output: {filename}'):
        output.excitation_energies


def test_oscillator_strengths_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    filename = (shared_datadir / 'empty_file')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(Exception, match=f'Could not find oscillator strengths in output: {filename}'):
        output.oscillator_strengths


def test_two_photon_strengths_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    filename = (shared_datadir / 'empty_file')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(Exception, match=f'Could not find two-photon strengths in output: {filename}'):
        output.two_photon_strengths


def test_two_photon_cross_sections_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    filename = (shared_datadir / 'empty_file')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(Exception, match=f'Could not find two-photon cross-sections in output: {filename}'):
        output.two_photon_cross_sections


def test_natural_occupations_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    filename = (shared_datadir / 'trigger_exceptions')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(Exception, match=f'Could not find natural occupations in output: {filename}'):
        output.natural_occupations


def test_hyperfine_coupling_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    filename = (shared_datadir / 'empty_file')
    output = dp.PostProcess(dalton, filename)
    with pytest.raises(Exception, match=f'Hyperfine couplings not found in output: {filename}'):
        output.hyperfine_coupling


def test_one_photon_excitation_input(shared_datadir):
    with open((shared_datadir / 'singlet_excitation.dal'), 'r') as file:
        reference = file.read()
    prop = dp.Property()
    prop.excitation_energy(states=[1, 2, 3, 4])
    input_file = dp.dalton_ifc.dalton_input(dp.QCMethod('HF'), prop)
    assert input_file == reference
    with open((shared_datadir / 'triplet_excitation_nosym.dal'), 'r') as file:
        reference = file.read()
    prop = dp.Property()
    prop.excitation_energy(states=5, triplet=True)
    input_file = dp.dalton_ifc.dalton_input(dp.QCMethod('HF'), prop)
    assert input_file == reference


def test_two_photon_excitation_input(shared_datadir):
    with open((shared_datadir / '2pa.dal'), 'r') as file:
        reference = file.read()
    prop = dp.Property()
    prop.two_photon_absorption(states=[1, 2, 3, 4])
    input_file = dp.dalton_ifc.dalton_input(dp.QCMethod('HF'), prop)
    assert input_file == reference
    with open((shared_datadir / '2pa_nosym.dal'), 'r') as file:
        reference = file.read()
    prop = dp.Property()
    prop.two_photon_absorption(states=5)
    input_file = dp.dalton_ifc.dalton_input(dp.QCMethod('HF'), prop)
    assert input_file == reference


@pytest.mark.parametrize(
    'method, properties, expected',
    [
        (
            ('HF', ),
            {'energy': True},
            textwrap.dedent("""\
                **DALTON INPUT
                .DIRECT
                .RUN WAVE FUNCTION
                **WAVE FUNCTIONS
                .HF
                *ORBITAL INPUT
                .PUNCHOUTORBITALS
                **END OF DALTON INPUT
                """),
        ),
        (
            ('DFT', 'B3LYP'),
            {'energy': True},
            textwrap.dedent("""\
                **DALTON INPUT
                .DIRECT
                .RUN WAVE FUNCTION
                **WAVE FUNCTIONS
                .DFT
                B3LYP
                *ORBITAL INPUT
                .PUNCHOUTORBITALS
                **END OF DALTON INPUT
                """),
        ),
        (
            ('DFT', 'BP86'),
            {'hfc_atoms': [1, 4]},
            textwrap.dedent("""\
                **DALTON INPUT
                .DIRECT
                .RUN RESPONSE
                **INTEGRALS
                .FC
                **RESPONSE
                .TRPFLG
                *ESR
                .ATOMS
                2
                1 4
                .FCCALC
                **WAVE FUNCTIONS
                .DFT
                BP86
                *ORBITAL INPUT
                .PUNCHOUTORBITALS
                **END OF DALTON INPUT
                """),
        ),
        (
            ('MP2', ),
            {'energy': True},
            textwrap.dedent("""\
                **DALTON INPUT
                .DIRECT
                .RUN WAVE FUNCTION
                **WAVE FUNCTIONS
                .HF
                .MP2
                *ORBITAL INPUT
                .PUNCHOUTORBITALS
                **END OF DALTON INPUT
                """),
        ),
    ],
    ids=['hf-energy', 'dft-energy', 'dft-hfc', 'mp2-energy'],
)
def test_dalton_input(method, properties, expected):
    generated = dp.dalton_ifc.dalton_input(dp.qcmethod.QCMethod(*method), dp.property.Property(**properties))
    assert generated == expected
