import numpy as np
import pytest

import daltonproject as dp


def test_opa_convolution(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, shared_datadir / 'He_opa')
    frequencies = np.linspace(47.5, 55.0, 1000)
    max_abs = np.max(output.convolute_opa(frequencies, broadening_function='gaussian', broadening_factor=0.4))
    assert np.allclose(max_abs, 76270.57678909194)
    max_abs = np.max(output.convolute_opa(frequencies, broadening_function='lorentzian', broadening_factor=0.4))
    assert np.allclose(max_abs, 51685.448765250156)


def test_opa_convolution_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, shared_datadir / 'He_opa')
    frequencies = np.linspace(47.5, 55.0, 1000)
    with pytest.raises(ValueError, match='The given name of broadening function; linear; is not valid.'):
        output.convolute_opa(frequencies, broadening_function='linear', broadening_factor=0.4)


def test_tpa_convolution(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, shared_datadir / 'He_tpa')
    frequencies = np.linspace(65.5, 67.0, 1000)
    max_abs = np.max(output.convolute_tpa(frequencies, broadening_function='lorentzian', broadening_factor=0.1))
    assert np.allclose(max_abs, 14.90279911808018)
    max_abs = np.max(output.convolute_tpa(frequencies, broadening_function='gaussian', broadening_factor=0.1))
    assert np.allclose(max_abs, 21.993157530081167)


def test_tpa_convolution_exceptions(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, shared_datadir / 'He_tpa')
    frequencies = np.linspace(65.5, 67.0, 1000)
    with pytest.raises(ValueError, match='The given name of broadening function; linear; is not valid.'):
        output.convolute_tpa(frequencies, broadening_function='linear', broadening_factor=0.1)
