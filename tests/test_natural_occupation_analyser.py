import pytest

import daltonproject as dp
from daltonproject.natural_occupation_analyser import NaturalOccupationAnalyser


def test_pick_cas_by_thresholds(shared_datadir):
    dalton = dp.QCProgram(program='Dalton')
    output = dp.PostProcess(dalton, shared_datadir / 'mp2_Ethylene')
    ref_electrons, ref_cas, ref_inactive = (4, [0, 1, 1, 0, 0, 1, 1, 0], [3, 1, 0, 0, 2, 0, 0, 0])
    electrons, cas, inactive = output.pick_cas_by_thresholds(1.98, 0.01)
    assert electrons == ref_electrons
    assert cas == ref_cas
    assert inactive == ref_inactive


def test_trimmed_natural_occ(shared_datadir):
    output = dp.PostProcess(dp.QCProgram(program='Dalton'), shared_datadir / 'mp2_He')
    out_string = output.trim_natural_occupations()
    ref_string = 'Symmetry: 1\n\n1.9909   0.0048   \n\nSymmetry: 2\n\n\nSymmetry: 3\n\n\nSymmetry: 4\n\n\nSymmetry: 5\n\n\nSymmetry: 6\n\n\nSymmetry: 7\n\n\nSymmetry: 8\n'
    assert out_string == ref_string


def test_scan_occupations(shared_datadir):
    output = dp.PostProcess(dp.QCProgram(program='Dalton'), shared_datadir / 'mp2_He')
    out_string = output.scan_occupations()
    ref_string = 'Strongly occupied natural orbitals                      Weakly occupied natural orbitals\nSymmetry   Occupation   Change in occ.   Diff. to 2     Symmetry   Occupation   Change in occ.\n   1          1.9909        0.0000        0.0091            1          0.0048        0.0000\n'
    assert out_string == ref_string
    # Test that the scan works when n_active < n_virtual, with n_virtual having values larger than 0.01.
    output = dp.PostProcess(dp.QCProgram(program='Dalton'), shared_datadir / 'mp2srdft_Be')
    ref_string = 'Strongly occupied natural orbitals                      Weakly occupied natural orbitals\nSymmetry   Occupation   Change in occ.   Diff. to 2     Symmetry   Occupation   Change in occ.\n   1          1.9648        0.0000        0.0352            1          0.0112        0.0000\n---------------------------------------------------         1          0.0112        0.0000\n---------------------------------------------------         1          0.0112        0.0000\n'
    assert output.scan_occupations() == ref_string


def test_NaturalOccupationAnalyser_exceptions():
    nat_picker = NaturalOccupationAnalyser([2.0, 0.0], strong_threshold=2.0, weak_threshold=0.0)
    assert nat_picker.strong_threshold == 2.0
    assert nat_picker.weak_threshold == 0.0
    with pytest.raises(TypeError, match='strong_threshold must be given as a float.'):
        nat_picker = NaturalOccupationAnalyser([2.0, 0.0], strong_threshold='2')
    with pytest.raises(TypeError, match='weak_threshold must be given as a float.'):
        nat_picker = NaturalOccupationAnalyser([2.0, 0.0], weak_threshold='0')
    with pytest.raises(TypeError, match='natural_occupations must either be a list or a dict.'):
        nat_picker = NaturalOccupationAnalyser((2.0, 0.0))
