import pytest

import daltonproject as dp


def test_property_init():
    properties = dp.Property(
        energy=True,
        gradient=True,
        hessian=True,
        geometry_optimization=True,
        excitation_energy=True,
    )
    assert 'energy' in properties.settings
    assert properties.settings['energy']
    assert 'gradient' in properties.settings
    assert properties.settings['gradient'] == 'analytic'
    assert 'hessian' in properties.settings
    assert properties.settings['hessian'] == 'analytic'
    assert 'geometry_optimization' in properties.settings
    assert properties.settings['geometry_optimization'] == 'BFGS'
    assert 'excitation_energy' in properties.settings
    assert properties.settings['excitation_energy'] == [5]


def test_property_energy():
    properties = dp.Property()
    properties.energy()
    assert 'energy' in properties.settings
    assert properties.settings['energy']


def test_property_gradient():
    properties = dp.Property()
    properties.gradient(method='numeric')
    assert 'gradient' in properties.settings
    assert properties.settings['gradient'] == 'numeric'


def test_property_hessian():
    properties = dp.Property()
    properties.hessian(method='numeric')
    assert 'hessian' in properties.settings
    assert properties.settings['hessian'] == 'numeric'


def test_property_geometry_optimization():
    properties = dp.Property()
    properties.geometry_optimization(method='DFP')
    assert 'geometry_optimization' in properties.settings
    assert properties.settings['geometry_optimization'] == 'DFP'


def test_property_excitation_energies():
    properties = dp.Property()
    properties.excitation_energy(states=2, triplet=True)
    assert 'excitation_energy' in properties.settings
    assert properties.settings['excitation_energy'] == [2]
    assert properties.settings['excitation_energy_triplet']
    properties.excitation_energy(states=[1, 2])
    assert properties.settings['excitation_energy'] == [1, 2]


def test_property_two_photon_absorption():
    properties = dp.Property()
    properties.two_photon_absorption(states=2)
    assert 'two_photon_absorption' in properties.settings
    assert properties.settings['two_photon_absorption'] == [2]
    properties.two_photon_absorption(states=[1, 2])
    assert properties.settings['two_photon_absorption'] == [1, 2]


def test_hyperfine_coupling():
    properties = dp.Property()
    properties.hyperfine_coupling(atoms=[1])
    assert 'hyperfine_coupling' in properties.settings


def test_dipole_gradient():
    properties = dp.Property()
    properties.dipole_gradient()
    assert 'dipole_gradient' in properties.settings
    assert properties.settings['dipole_gradient']


def test_polarizability_gradient():
    properties = dp.Property(polarizability_gradient=True)
    assert 'polarizability_gradient' in properties.settings
    assert properties.settings['polarizability_gradient_frequencies'] == [0.0]
    properties.polarizability_gradient(frequencies=[0.0, 0.1])
    assert properties.settings['polarizability_gradient']
    assert properties.settings['polarizability_gradient_frequencies'] == [0.0, 0.1]


def test_property_exceptions():
    properties = dp.Property()
    with pytest.raises(TypeError, match='Gradient type must be given as a string'):
        properties.gradient(method=['analytic'])
    with pytest.raises(TypeError, match='Hessian type must be given as a string'):
        properties.hessian(method=2)
    with pytest.raises(TypeError, match='Optimization method must be given as a string'):
        properties.geometry_optimization(method={'BFGS': True})
    with pytest.raises(
            TypeError,
            match='Number of states must be given as an integer or list of integers if molecular symmetry is used'):
        properties.excitation_energy(states=5.0)
    with pytest.raises(TypeError, match='Triplet can be either True or False'):
        properties.excitation_energy(triplet=1)
    with pytest.raises(
            TypeError,
            match='Number of states must be given as an integer or list of integers if molecular symmetry is used'):
        properties.two_photon_absorption(states='5')
    with pytest.raises(
            TypeError,
            match='Number of states must be given as an integer or list of integers if molecular symmetry is used'):
        properties.two_photon_absorption(states=5.0)
    with pytest.raises(
            TypeError,
            match='Number of states must be given as an integer or list of integers if molecular symmetry is used'):
        properties.two_photon_absorption(states=None)
    with pytest.raises(
            TypeError,
            match='Number of states must be given as an integer or list of integers if molecular symmetry is used'):
        properties.two_photon_absorption(states={5: 5})
    with pytest.raises(
            TypeError,
            match='Number of states must be given as an integer or list of integers if molecular symmetry is used'):

        def f():
            return 5

        properties.two_photon_absorption(states=f)
    with pytest.raises(
            TypeError,
            match='Frequencies must be given as a float of a list of floats if default values is not desired.'):
        properties.polarizability_gradient(frequencies=1)
