import numpy as np
import pytest

import daltonproject as dp


def test_molecule_init_string():
    # Init molecule with atoms given as a string using ";" as atom separator.
    ref_elements = ['O', 'H', 'H']
    ref_coordinates = [[1.0, 2.0, 0.5], [0.0, 1.0, 1.0], [-1.0, 0.5, 4.0]]
    atoms = 'O 1.0 2.0 0.5; H 0.0 1.0 1.0; H -1.0 0.5 4.0'
    molecule = dp.Molecule(atoms=atoms)
    assert isinstance(molecule.charge, int)
    assert molecule.charge == 0
    assert isinstance(molecule.elements, list)
    for element in molecule.elements:
        assert isinstance(element, str)
    assert molecule.elements == ref_elements
    assert molecule.labels == ref_elements
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate
    # Init molecule with a single atom.
    ref_elements = ['O']
    ref_coordinates = [[1.0, 2.0, 0.5]]
    atom = 'O 1.0 2.0 0.5'
    molecule = dp.Molecule(atoms=atom)
    assert isinstance(molecule.charge, int)
    assert molecule.charge == 0
    assert isinstance(molecule.elements, list)
    for element in molecule.elements:
        assert isinstance(element, str)
    assert molecule.elements == ref_elements
    assert molecule.labels == ref_elements
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate
    # Init molecule with atoms given as a string using "\n" as atom separator and adding atom labels.
    ref_elements = ['O', 'H', 'H']
    ref_labels = ['O1', 'H1', 'H2']
    ref_coordinates = [[1.0, 2.0, 0.5], [0.0, 1.0, 1.0], [-1.0, 0.5, 4.0]]
    atoms = 'O 1.0 2.0 0.5 O1\n H 0.0 1.0 1.0 H1\n H -1.0 0.5 4.0 H2'
    molecule = dp.Molecule(atoms=atoms, charge=-1)
    assert isinstance(molecule.charge, int)
    assert molecule.charge == -1
    assert isinstance(molecule.elements, list)
    for element in molecule.elements:
        assert isinstance(element, str)
    assert molecule.elements == ref_elements
    assert molecule.labels == ref_labels
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate


def test_molecule_init_xyz_string(shared_datadir):
    ref_elements = ['C', 'H', 'H', 'H', 'C', 'H', 'H', 'O', 'H']
    ref_labels = ['C1', 'H1', 'H2', 'H3', 'C2', 'H4', 'H5', 'O', 'H6']
    ref_coordinates = [
        [9.136479, 11.724970, 51.500927],
        [9.294462, 12.628635, 50.910741],
        [8.160480, 11.311733, 51.245960],
        [9.126987, 11.998458, 52.556137],
        [10.230421, 10.715737, 51.222941],
        [11.209335, 11.140253, 51.472204],
        [10.240910, 10.452121, 50.159449],
        [9.976946, 9.554726, 52.018512],
        [10.663980, 8.903366, 51.853128],
    ]
    ethanol = shared_datadir / 'ethanol.xyz'
    molecule = dp.Molecule(xyz=ethanol, charge=+4)
    assert isinstance(molecule.charge, int)
    assert molecule.charge == 4
    assert isinstance(molecule.elements, list)
    for element in molecule.elements:
        assert isinstance(element, str)
    assert molecule.elements == ref_elements
    assert molecule.labels == ref_labels
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate


def test_molecule_init_xyz_file(shared_datadir):
    ref_elements = ['C', 'H', 'H', 'H', 'C', 'H', 'H', 'O', 'H']
    ref_labels = ['C1', 'H1', 'H2', 'H3', 'C2', 'H4', 'H5', 'O', 'H6']
    ref_coordinates = [
        [9.136479, 11.724970, 51.500927],
        [9.294462, 12.628635, 50.910741],
        [8.160480, 11.311733, 51.245960],
        [9.126987, 11.998458, 52.556137],
        [10.230421, 10.715737, 51.222941],
        [11.209335, 11.140253, 51.472204],
        [10.240910, 10.452121, 50.159449],
        [9.976946, 9.554726, 52.018512],
        [10.663980, 8.903366, 51.853128],
    ]
    molecule = dp.Molecule(xyz=(shared_datadir / 'ethanol.xyz'), charge=+4)
    assert isinstance(molecule.charge, int)
    assert molecule.charge == 4
    assert isinstance(molecule.elements, list)
    for element in molecule.elements:
        assert isinstance(element, str)
    assert molecule.elements == ref_elements
    assert molecule.labels == ref_labels
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate


def test_molecule_init_exceptions():
    with pytest.raises(TypeError, match='Charge must be an integer.'):
        dp.Molecule(charge=0.0)
    with pytest.raises(ValueError, match='Incompatible input: O 1.0 2.0 0.5 O1: H 0.0 1.0 1.0 H1: H -1.0 0.5 4.0 H2'):
        atoms = 'O 1.0 2.0 0.5 O1: H 0.0 1.0 1.0 H1: H -1.0 0.5 4.0 H2'
        dp.Molecule(atoms=atoms)
    with pytest.raises(ValueError, match='Incompatible input: O 1.0 2.0'):
        atoms = 'O 1.0 2.0'
        dp.Molecule(atoms=atoms)
    with pytest.raises(ValueError, match='Incompatible input: O 1.0 2.0 0.5 O1 O2'):
        atoms = 'O 1.0 2.0 0.5 O1 O2'
        dp.Molecule(atoms=atoms)
    with pytest.raises(ValueError, match='Unknown element: Xy.'):
        atoms = 'O 1.0 2.0 0.5; Xy 0.0 0.0 0.0'
        dp.Molecule(atoms=atoms)
    with pytest.raises(TypeError, match='Atoms are given as a string using either'):
        atoms = ['O 1.0 2.0 0.5 O1; H 0.0 1.0 1.0 H1; H -1.0 0.5 4.0 H2']
        dp.Molecule(atoms=atoms)
    with pytest.raises(SyntaxError, match='Specify either atoms or xyz.'):
        dp.Molecule(atoms='O 0.0 0.0 0.0', xyz='test.xyz')


def test_molecule_xyz_file(shared_datadir):
    ref_elements = ['O', 'H', 'H']
    ref_labels = ['O', 'H', 'H']
    ref_coordinates = [
        [0.000000, 0.000000, 0.000000],
        [0.758602, 0.000000, 0.504284],
        [0.758602, 0.000000, -0.504284],
    ]
    molecule = dp.Molecule()
    assert isinstance(molecule.charge, int)
    assert molecule.charge == 0
    molecule.xyz_reader(shared_datadir / 'water.xyz')
    assert isinstance(molecule.elements, list)
    for element in molecule.elements:
        assert isinstance(element, str)
    assert molecule.elements == ref_elements
    assert molecule.labels == ref_labels
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate


def test_molecule_xyz_exception():
    molecule = dp.Molecule()
    with pytest.raises(FileNotFoundError):
        molecule.xyz_reader('test.xyz')


def test_molecule_coordinates(shared_datadir):
    ref_coordinates = [
        [0.000000, 0.000000, 0.000000],
        [0.758602, 0.000000, 0.504284],
        [0.758602, 0.000000, -0.504284],
    ]
    molecule = dp.Molecule(xyz=(shared_datadir / 'water.xyz'), charge=+4)
    # Test with list of coordinates as input.
    molecule.coordinates = ref_coordinates
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate
    # Test with numpy array of coordinates as input.
    molecule.coordinates = np.array(ref_coordinates)
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate


def test_molecule_coordinates_exceptions(shared_datadir):
    wrong_length = [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]]
    molecule = dp.Molecule(xyz=(shared_datadir / 'water.xyz'), charge=+4)
    with pytest.raises(Exception, match='Number of coordinates does not correspond to number of atoms in molecule.'):
        molecule.coordinates = wrong_length


def test_molecule_num_atoms():
    atoms = 'O 1.0 2.0 0.5; H 0.0 1.0 1.0; H -1.0 0.5 4.0'
    molecule = dp.Molecule(atoms=atoms)
    assert molecule.num_atoms == 3


def test_xyz_input_newlines(shared_datadir):
    """Tests that an xyz-file can loaded when it got trailing newlines.
    """
    molecule_file = (shared_datadir / 'Be_newlines.xyz')
    molecule = dp.Molecule(xyz=molecule_file)
    assert abs(molecule.coordinates[0, 0]) < 1e-12
    assert abs(molecule.coordinates[0, 1]) < 1e-12
    assert abs(molecule.coordinates[0, 2]) < 1e-12


def test_xyz_input_no_newline(shared_datadir):
    """Tests that an xyz-file can loaded when no newline in the end.
    """
    molecule_file = (shared_datadir / 'Be_no_newline.xyz')
    molecule = dp.Molecule(xyz=molecule_file)
    assert abs(molecule.coordinates[0, 0]) < 1e-12
    assert abs(molecule.coordinates[0, 1]) < 1e-12
    assert abs(molecule.coordinates[0, 2]) < 1e-12


def test_broken_xyz_files(shared_datadir):
    """Tests that broken xyz-files get the right errors.
    """
    with pytest.raises(TypeError, match='First line in xyz file cannot be converted to an integer'):
        molecule_file = shared_datadir / 'Be_brokenfile.xyz'
        dp.Molecule(xyz=molecule_file)
    with pytest.warns(
            UserWarning,
            match='Only 1 atoms were declared but xyz file apparently contains 2 lines. Only 1 will be read.'):
        molecule_file = shared_datadir / 'Be_brokenfile2.xyz'
        dp.Molecule(xyz=molecule_file)
    with pytest.raises(ValueError, match='2 atoms were declared, but file only contains 1 lines.'):
        molecule_file = shared_datadir / 'Be_brokenfile3.xyz'
        dp.Molecule(xyz=molecule_file)
    with pytest.raises(ValueError, match='Unknown element in line:\neB 0 0 0.'):
        molecule_file = shared_datadir / 'Be_brokenfile4.xyz'
        dp.Molecule(xyz=molecule_file)
    with pytest.raises(ValueError, match='Wrong number of values in line:\nBe 0 0'):
        molecule_file = shared_datadir / 'Be_brokenfile5.xyz'
        dp.Molecule(xyz=molecule_file)
    with pytest.raises(TypeError, match='Cannot read coordinates from line:\nBe 0 0 a'):
        molecule_file = shared_datadir / 'Be_brokenfile6.xyz'
        dp.Molecule(xyz=molecule_file)
    with pytest.raises(TypeError, match='Cannot read coordinates from line:\nBe 0 a 0'):
        molecule_file = shared_datadir / 'Be_brokenfile7.xyz'
        dp.Molecule(xyz=molecule_file)
    with pytest.raises(TypeError, match='Cannot read coordinates from line:\nBe a 0 0'):
        molecule_file = shared_datadir / 'Be_brokenfile8.xyz'
        dp.Molecule(xyz=molecule_file)
