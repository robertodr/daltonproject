import pytest

import daltonproject as dp
from daltonproject.dalton_ifc import DaltonIFC
from daltonproject.lsdalton_ifc import LSDaltonIFC


def test_qcprogram_dalton():
    dalton = dp.QCProgram('Dalton')
    assert isinstance(dalton, DaltonIFC)


def test_qcprogram_lsdalton():
    lsdalton = dp.QCProgram('LSDalton')
    assert isinstance(lsdalton, LSDaltonIFC)


def test_qcprogram_exceptions():
    with pytest.raises(TypeError, match='Program must be given as a string'):
        dp.QCProgram(['Dalton'])
    with pytest.raises(ValueError, match='Program Dirac is not supported'):
        dp.QCProgram('Dirac')
