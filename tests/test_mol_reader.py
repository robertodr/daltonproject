import numpy as np
import pytest

import daltonproject as dp


def test_atombasis(shared_datadir):
    ref_coordinates = np.array([
        [0.00000000, 0.00000000, 0.00000000],
        [1.7314309298043, 1.7314309298043, -1.7314309298043],
        [-1.7314309298043, 1.7314309298043, 1.7314309298043],
        [1.7314309298043, -1.7314309298043, 1.7314309298043],
        [-1.7314309298043, -1.7314309298043, -1.7314309298043],
    ])
    ref_basis_set = {'Mn': 'aug-cc-pVTZ-J', 'O': 'aug-cc-pVTZ-J'}
    ref_charge = -1
    molecule, basis = dp.mol_reader(shared_datadir / 'MnO4.mol')
    assert molecule.coordinates == pytest.approx(ref_coordinates)
    assert ref_charge == molecule.charge
    assert len(ref_basis_set) == len(basis.basis)
    for key in ref_basis_set:
        assert ref_basis_set[key] == basis.basis[key]
    molecule, basis = dp.mol_reader(shared_datadir / 'MnO4_generator.mol')
    assert molecule.coordinates == pytest.approx(ref_coordinates)
    assert ref_charge == molecule.charge
    assert len(ref_basis_set) == len(basis.basis)
    for key in ref_basis_set:
        assert ref_basis_set[key] == basis.basis[key]


def test_basis(shared_datadir):
    ref_coordinates = np.array([
        [0.0, 0.0, -1.13698349],
        [0.0, 0.0, 1.13429162],
        [0.0, 1.76355276, -2.23208171],
        [0.0, -1.76355276, -2.23208171],
    ])
    ref_basis_set = 'aug-cc-pVTZ'
    ref_charge = 0
    molecule, basis = dp.mol_reader(shared_datadir / 'Formaldehyde.mol')
    assert molecule.coordinates == pytest.approx(ref_coordinates)
    assert ref_charge == molecule.charge
    assert ref_basis_set == basis.basis
    molecule, basis = dp.mol_reader(shared_datadir / 'Formaldehyde_generator.mol')
    assert molecule.coordinates == pytest.approx(ref_coordinates)


def test_noncanonical_d2h(shared_datadir):
    ref_coordinates = np.array([[0.0, -0.787674, -0.776833], [0.0, -0.787674, 0.776833], [0.0, 0.787674, 0.776833],
                                [0.0, 0.787674, -0.776833], [1.406149, -1.14826, -1.277928],
                                [1.406149, -1.14826, 1.277928], [1.406149, 1.14826, 1.277928],
                                [1.406149, 1.14826, -1.277928], [-1.406149, -1.14826, -1.277928],
                                [-1.406149, -1.14826, 1.277928], [-1.406149, 1.14826, 1.277928],
                                [-1.406149, 1.14826, -1.277928], [-1.768334, 0.0, 2.247798],
                                [1.768334, 0.0, 2.247798], [-1.768334, 0.0, -2.247798], [1.768334, 0.0, -2.247798],
                                [-2.825488, 0.0, 2.532426], [2.825488, 0.0, 2.532426], [-2.825488, 0.0, -2.532426],
                                [2.825488, 0.0, -2.532426], [-1.169485, 0.0, 3.162622], [1.169485, 0.0, 3.162622],
                                [-1.169485, 0.0, -3.162622], [1.169485, 0.0, -3.162622], [2.231222, 0.797312, 0.0],
                                [-2.231222, 0.797312, 0.0], [2.231222, -0.797312, 0.0], [-2.231222, -0.797312, 0.0],
                                [3.239787, 1.215692, 0.0], [-3.239787, 1.215692, 0.0], [3.239787, -1.215692, 0.0],
                                [-3.239787, -1.215692, 0.0], [1.546986, -2.164883, -1.650606],
                                [1.546986, -2.164883, 1.650606], [1.546986, 2.164883, 1.650606],
                                [1.546986, 2.164883, -1.650606], [-1.546986, -2.164883, -1.650606],
                                [-1.546986, -2.164883, 1.650606], [-1.546986, 2.164883, 1.650606],
                                [-1.546986, 2.164883, -1.650606]])
    ref_basis_set = {'C': '3-21G', 'H': '3-21G'}
    ref_charge = 0
    molecule, basis = dp.mol_reader(shared_datadir / 'Pagodane_Z_X_Y.mol')
    assert np.all(abs(ref_coordinates - molecule.coordinates)) < 1e-12
    assert ref_charge == molecule.charge
    assert ref_basis_set == basis.basis
    molecule, basis = dp.mol_reader(shared_datadir / 'Pagodane_Y_Z_X.mol')
    assert np.all(abs(ref_coordinates - molecule.coordinates)) < 1e-12
