import numpy as np

import daltonproject as dp
import daltonproject.spectrum

dalton = dp.QCProgram(program='Dalton')
hf = dp.QCMethod('HF')
basis = dp.Basis(basis='pcseg-0')
energy = dp.Property(two_photon_absorption=True)
water = dp.Molecule(xyz='data/water.xyz', charge=0)
result = dalton.compute(hf, basis, water, energy, force_rerun=True)
output = dp.PostProcess(dalton, result)

frequencies = np.linspace(0.0, 10.0, 1000)
spectrum = output.convolute_tpa(output, frequencies)
print(spectrum)
