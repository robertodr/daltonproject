import daltonproject as dp

dalton = dp.QCProgram('dalton')
mol = dp.Molecule(atoms='N 0 0 0')
bas = dp.Basis('Huz-IVsu4')
qc = dp.QCMethod('DFT', 'B3LYP')
prop = dp.Property(energy=True, hfc_atoms=[1])
proc = dalton.compute(qc, bas, mol, prop)
post = dp.PostProcess(dalton, proc)
print(post.hyperfine_coupling)
