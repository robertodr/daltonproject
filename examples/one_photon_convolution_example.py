import matplotlib.pyplot as plt
import numpy as np

import daltonproject as dp
import daltonproject.spectrum

dalton = dp.QCProgram(program='Dalton')
hf = dp.QCMethod('HF')
basis = dp.Basis(basis='pcseg-0')
energy = dp.Property(excitation_energy=True)
water = dp.Molecule(xyz='data/water.xyz', charge=0)
result = dalton.compute(hf, basis, water, energy)
output = dp.PostProcess(dalton, result)

frequencies = np.linspace(6.0, 20.0, 1000)
spectrum = output.convolute_opa(output, frequencies)

plt.plot(frequencies, spectrum)
plt.show()
