import daltonproject as dp

dalton = dp.QCProgram(program='Dalton')
lsdalton = dp.QCProgram(program='LSDalton')

hf = dp.QCMethod('HF')
dft = dp.QCMethod('DFT', 'B3LYP')
small_basis = dp.Basis(basis='pcseg-0')
larger_basis = dp.Basis(basis='pcseg-1')
largest_basis = dp.Basis(basis='pcseg-2')
ethanol = dp.Molecule(xyz='../sample_molecules/ethanol.xyz', charge=0)
geo_opt = dp.Property(geometry_optimization=True)
energy = dp.Property(energy=True)

result = lsdalton.compute(hf, small_basis, ethanol, geo_opt)
output = dp.PostProcess(lsdalton, result)
print(output.energy)
print(output.final_geometry)

ethanol.coordinates = output.final_geometry
result = lsdalton.compute(hf, larger_basis, ethanol, geo_opt)
output = dp.PostProcess(lsdalton, result)
print(output.energy)
print(output.final_geometry)

ethanol.coordinates = output.final_geometry
result = dalton.compute(hf, largest_basis, ethanol, energy)
output = dp.PostProcess(dalton, result)
print(output.energy)
print(output.mo_energies)
