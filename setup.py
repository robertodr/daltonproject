#!/usr/bin/env python
# coding=utf-8
import os.path as path

from setuptools import setup

import daltonproject

with open(path.join(path.abspath(path.dirname(__file__)), 'README.md'), encoding='utf-8') as readme_file:
    long_description = readme_file.read()
description = 'Dalton Project: Providing Python access to simulations of molecular and electronic structures of complex systems'

setup(
    name='Dalton Project',
    version=daltonproject.__version__,
    description=description,
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/DaltonProject/daltonproject',
    # download_url='https://pypi.org/project/DaltonProject/',
    author='Dalton Project developers',
    license='GPLv3+',
    classifiers=[
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)', 'Natural Language :: English',
        'Programming Language :: Python :: 3.6', 'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8', 'Topic :: Scientific/Engineering :: Chemistry',
        'Topic :: Scientific/Engineering :: Physics'
    ],
    install_requires=['numpy'],
    python_requires='>=3.6',
    packages=['daltonproject'],
)
