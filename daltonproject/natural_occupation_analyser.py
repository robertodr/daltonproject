from typing import Dict, List, Tuple, Union

import numpy as np


class NaturalOccupationAnalyser:
    """Analysis of natural occupation numbers.
    """
    def __init__(self,
                 natural_occupations: Union[np.ndarray, Dict[int, np.ndarray]],
                 strong_threshold: float = 1.995,
                 weak_threshold: float = 0.0025) -> None:
        """Initialize NaturalOccupationAnalyser class.

        Args:
          natural_occupations: Natural occupation numbers.
          strong_threshold: Strongly occupied natural orbitals with occupations above this threshold will not be printed.
          weak_threshold: Weakly occupied natural orbitals with occupations below this threshold will not be printed.
        """
        if not isinstance(strong_threshold, float):
            raise TypeError('strong_threshold must be given as a float.')
        if not isinstance(weak_threshold, float):
            raise TypeError('weak_threshold must be given as a float.')
        self.strong_threshold = strong_threshold
        self.weak_threshold = weak_threshold
        if isinstance(natural_occupations, list):
            natural_occupations = {1: natural_occupations}
        if not isinstance(natural_occupations, dict):
            raise TypeError('natural_occupations must either be a list or a dict.')
        self.num_symmetries = 1
        for key in natural_occupations:
            natural_occupations[key] = np.array(natural_occupations[key])
            self.num_symmetries = np.max([key, self.num_symmetries])
        # Strongly occupied.
        self.strong_nat_occ = {}
        # Weakly occupied.
        self.weak_nat_occ = {}
        for key in natural_occupations:
            # Strong occupations have smallest numbers first.
            self.strong_nat_occ[key] = np.sort(natural_occupations[key][natural_occupations[key] >= 1.0])
            # Weak occupations have largest numbers first.
            self.weak_nat_occ[key] = np.sort(natural_occupations[key][natural_occupations[key] < 1.0])[::-1]
        # ------------------------------------------------------------------------------------------------------------
        # Make np.ndarrays of the natural occupations without symmetry, and an array keeping track of their symmetry.
        # ------------------------------------------------------------------------------------------------------------
        self.strong_nat_occ_nosym: List[float] = []
        self.strong_nat_occ_nosym2sym_idx: List[int] = []
        for key in self.strong_nat_occ:
            self.strong_nat_occ_nosym = self.strong_nat_occ_nosym + self.strong_nat_occ[key].tolist()
            self.strong_nat_occ_nosym2sym_idx = self.strong_nat_occ_nosym2sym_idx + [key] * len(
                self.strong_nat_occ[key])
        self.weak_nat_occ_nosym: List[float] = []
        self.weak_nat_occ_nosym2sym_idx: List[int] = []
        for key in self.weak_nat_occ:
            self.weak_nat_occ_nosym = self.weak_nat_occ_nosym + self.weak_nat_occ[key].tolist()
            self.weak_nat_occ_nosym2sym_idx = self.weak_nat_occ_nosym2sym_idx + [key] * len(self.weak_nat_occ[key])
        self.strong_nat_occ_nosym = np.array(self.strong_nat_occ_nosym)
        self.strong_nat_occ_nosym2sym_idx = np.array(self.strong_nat_occ_nosym2sym_idx)
        self.weak_nat_occ_nosym = np.array(self.weak_nat_occ_nosym)
        self.weak_nat_occ_nosym2sym_idx = np.array(self.weak_nat_occ_nosym2sym_idx)
        # Make sure the arrays are correctly sorted.
        idx = np.argsort(self.strong_nat_occ_nosym)
        self.strong_nat_occ_nosym = self.strong_nat_occ_nosym[idx]
        self.strong_nat_occ_nosym2sym_idx = self.strong_nat_occ_nosym2sym_idx[idx]
        idx = np.argsort(self.weak_nat_occ_nosym)[::-1]
        self.weak_nat_occ_nosym = self.weak_nat_occ_nosym[idx]
        self.weak_nat_occ_nosym2sym_idx = self.weak_nat_occ_nosym2sym_idx[idx]

    def trimmed_natural_occ(self) -> str:
        """Print relevant natural occupations in a formatted way.

        Returns
          String to be printed or saved to file.
        """
        out_string = ''
        for key in range(1, self.num_symmetries + 1):
            if key != 1:
                out_string += '\n\n'
            out_string += f'Symmetry: {key}\n'
            orbital_counter = 0
            for nat_occ in self.strong_nat_occ[key][::-1]:
                if nat_occ > self.strong_threshold:
                    continue
                if orbital_counter % 5 == 0:
                    out_string += '\n'
                orbital_counter += 1
                out_string += f'{nat_occ:.4f}   '
            for nat_occ in self.weak_nat_occ[key]:
                if nat_occ < self.weak_threshold:
                    continue
                if orbital_counter % 5 == 0:
                    out_string += '\n'
                orbital_counter += 1
                out_string += f'{nat_occ:.4f}   '
        return out_string

    def scan_occupations(self, max_orbitals: int = 14) -> str:
        """Determine thresholds for picking active space.
        Will not print orbitals outside the bounds of "strong_threshold" and "weak_threshold".

        Args:
          max_orbitals: Maximum number of strongly occupied and weakly occupied orbitals to print.

        Returns
          String to be printed or saved to file.
        """
        out_string = ''
        out_string += 'Strongly occupied natural orbitals                      Weakly occupied natural orbitals\n'
        out_string += 'Symmetry   Occupation   Change in occ.   Diff. to 2     Symmetry   Occupation   Change in occ.\n'
        orbital_counter = 0
        strong_previous_occupation = self.strong_nat_occ_nosym[0]
        weak_previous_occupation = self.weak_nat_occ_nosym[0]
        # Padding of the occupations is needed in case there are more relevant virtual orbitals than active orbitals.
        tmp_strong_nat_occ_nosym = self.strong_nat_occ_nosym.copy()
        tmp_strong_nat_occ_nosym2sym_idx = self.strong_nat_occ_nosym2sym_idx.copy()
        tmp_weak_nat_occ_nosym = self.weak_nat_occ_nosym.copy()
        tmp_weak_nat_occ_nosym2sym_idx = self.weak_nat_occ_nosym2sym_idx.copy()
        if len(tmp_weak_nat_occ_nosym) > len(tmp_strong_nat_occ_nosym):
            npad = len(tmp_weak_nat_occ_nosym)
            tmp_strong_nat_occ_nosym = np.pad(tmp_strong_nat_occ_nosym,
                                              pad_width=npad,
                                              mode='constant',
                                              constant_values=10)[npad:]
            tmp_strong_nat_occ_nosym2sym_idx = np.pad(tmp_strong_nat_occ_nosym2sym_idx,
                                                      pad_width=npad,
                                                      mode='constant',
                                                      constant_values=10)[npad:]
        elif len(tmp_weak_nat_occ_nosym) < len(tmp_strong_nat_occ_nosym):
            npad = len(tmp_strong_nat_occ_nosym)
            tmp_weak_nat_occ_nosym = np.pad(tmp_weak_nat_occ_nosym,
                                            pad_width=npad,
                                            mode='constant',
                                            constant_values=10)[npad:]
            tmp_weak_nat_occ_nosym2sym_idx = np.pad(tmp_weak_nat_occ_nosym2sym_idx,
                                                    pad_width=npad,
                                                    mode='constant',
                                                    constant_values=10)[npad:]
        for strong_occupation, strong_symmetry, weak_occupation, weak_symmetry in zip(
                tmp_strong_nat_occ_nosym, tmp_strong_nat_occ_nosym2sym_idx, tmp_weak_nat_occ_nosym,
                tmp_weak_nat_occ_nosym2sym_idx):
            if orbital_counter == max_orbitals:
                break
            elif strong_occupation > self.strong_threshold and weak_occupation < self.weak_threshold:
                break
            if strong_occupation < self.strong_threshold:
                out_string += f'{strong_symmetry:4}{strong_occupation:16.4f}{strong_occupation-strong_previous_occupation:14.4f}{2-strong_occupation:14.4f}   '
                strong_previous_occupation = strong_occupation
            else:
                out_string += f'---------------------------------------------------'
            if weak_occupation > self.weak_threshold:
                out_string += f'{weak_symmetry:10}{weak_occupation:16.4f}{weak_previous_occupation-weak_occupation:14.4f}'
                weak_previous_occupation = weak_occupation
            else:
                out_string += f'--------------------------------------------'
            orbital_counter += 1
            out_string += '\n'
        return out_string

    def pick_cas_by_thresholds(
            self, max_strong_occupation: float,
            min_weak_occupation: float) -> Tuple[int, Union[int, List[int]], Union[int, List[int]]]:
        """Get parameters needed for a CAS calculation based on simple thresholds.

        Args:
          max_strong_occupation: Maximum occupation number of strongly occupied natural orbitals to be included in CAS.
          min_weak_occupation: Minimum occupation number of weakly occupied natural orbitals to be included in CAS.

        Returns:
          Number active electrons, CAS, number inactive orbitals.
        """
        electrons = 0
        cas = [0] * self.num_symmetries
        inactive = [0] * self.num_symmetries
        for occupation, symmetry in zip(self.strong_nat_occ_nosym, self.strong_nat_occ_nosym2sym_idx):
            if occupation > max_strong_occupation:
                inactive[symmetry - 1] += 1
            else:
                cas[symmetry - 1] += 1
                electrons += 2
        for occupation, symmetry in zip(self.weak_nat_occ_nosym, self.weak_nat_occ_nosym2sym_idx):
            if occupation > min_weak_occupation:
                cas[symmetry - 1] += 1
            else:
                break
        if self.num_symmetries == 1:
            return electrons, cas[0], inactive[0]
        return electrons, cas, inactive
