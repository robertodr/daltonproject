from typing import Any, Dict, List, Union


class Property:
    """Define properties to be computed.
    """
    def __init__(self,
                 energy: bool = False,
                 gradient: bool = False,
                 hessian: bool = False,
                 geometry_optimization: bool = False,
                 excitation_energy: bool = False,
                 two_photon_absorption: bool = False,
                 hfc_atoms: List[int] = [],
                 dipole_gradient: bool = False,
                 polarizability_gradient: bool = False) -> None:
        """Initialize Property class.

        Args:
          energy: Activate energy calculation.
          gradient: Activate molecular gradient calculation.
          hessian: Activate molecular Hessian calculation.
          geometry_optimization: Activate geometry optimization.
          excitation_energy: Activate excitation energy calculation.
          two_photon_absorption: Activate two-photon absorption calculation.
          hyperfine_coupling: Activate hyperfine coupling calculation.
          dipole_gradient: Activate dipole gradient calculation.
          polarizability_gradient: Activate polarizability gradient calculation
        """
        self.settings: Dict[str, Any] = {}
        if energy:
            self.energy()
        if gradient:
            self.gradient()
        if hessian:
            self.hessian()
        if geometry_optimization:
            self.geometry_optimization()
        if excitation_energy:
            self.excitation_energy()
        if two_photon_absorption:
            self.two_photon_absorption()
        if hfc_atoms:
            self.hyperfine_coupling(hfc_atoms)
        if dipole_gradient:
            self.dipole_gradient()
        if polarizability_gradient:
            self.polarizability_gradient()

    def energy(self) -> None:
        """Energy.
        """
        self.settings.update({'energy': True})

    def gradient(self, method: str = 'analytic') -> None:
        """Compute molecular gradient.

        Args:
          method: Method for computing gradient.
        """
        if not isinstance(method, str):
            raise TypeError('Gradient type must be given as a string')
        self.settings.update({'gradient': method.lower()})

    def hessian(self, method: str = 'analytic') -> None:
        """Compute molecular Hessian.

        Args:
          method: Method for computing Hessian.
        """
        if not isinstance(method, str):
            raise TypeError('Hessian type must be given as a string')
        self.settings.update({'hessian': method.lower()})

    def geometry_optimization(self, method: str = 'BFGS') -> None:
        """Geometry Optimization.

        Args:
          method: Geometry optimization method.
        """
        if not isinstance(method, str):
            raise TypeError('Optimization method must be given as a string')
        self.settings.update({'geometry_optimization': method.upper()})

    def excitation_energy(self, states: Union[int, List[int]] = 5, triplet: bool = False) -> None:
        """Compute excitation energies

        Args:
          states: Number of states.
          triplet: Turns on triplet excitations. Excitations are singlet as default.
        """
        if isinstance(states, int):
            states = [states]
        if not isinstance(states, list):
            raise TypeError('Number of states must be given as an integer '
                            'or list of integers if molecular symmetry is used')
        if not isinstance(triplet, bool):
            raise TypeError('Triplet can be either True or False')
        self.settings.update({'excitation_energy': states})
        if triplet:
            self.settings.update({'excitation_energy_triplet': triplet})

    def two_photon_absorption(self, states: Union[int, List[int]] = 5) -> None:
        """Compute two-photon absorption strengths

        Args:
          states: Number of states.
        """
        if isinstance(states, int):
            states = [states]
        if not isinstance(states, list):
            raise TypeError('Number of states must be given as an integer '
                            'or list of integers if molecular symmetry is used')
        self.settings.update({'two_photon_absorption': states})

    def hyperfine_coupling(self, atoms: List[int]) -> None:
        """Compute hyperfine coupling constants.

        Args:
          atoms: List of atoms by index.
        """
        self.settings.update({'hyperfine_coupling': atoms})

    def dipole_gradient(self) -> None:
        """Compute dipole gradient
        """
        self.settings.update({'dipole_gradient': True})

    def polarizability_gradient(self, frequencies: Union[float, List[float]] = 0.0) -> None:
        """Compute polarizability gradient

        Args:
          frequencies: Incident energy (Eh).
        """
        self.settings.update({'polarizability_gradient': True})
        if isinstance(frequencies, float):
            frequencies = [frequencies]
        if not isinstance(frequencies, list):
            raise TypeError(
                'Frequencies must be given as a float of a list of floats if default values is not desired.')
        self.settings.update({'polarizability_gradient_frequencies': frequencies})
