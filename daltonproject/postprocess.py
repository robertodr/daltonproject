from typing import Callable, List, Optional, Tuple, Union

import numpy as np

from .dalton_ifc import DaltonIFC
from .lsdalton_ifc import LSDaltonIFC
from .natural_occupation_analyser import NaturalOccupationAnalyser
from .spectrum import convolute_one_photon_absorption, convolute_two_photon_absorption, gaussian, lorentzian


class PostProcess:
    """Class for handling post-processing of results produced by the given program.
    """
    def __init__(self, program: Union[DaltonIFC, LSDaltonIFC], output: str) -> None:
        """Initialize PostProcess class.

        Args:
          program: QCProgram object.
          output: Name of output files without extension.
        """
        self.program = program
        self.output = output
        self._nat_occ_analyser: Optional[NaturalOccupationAnalyser] = None

    @property
    def energy(self):
        return self.program.energy(self.output)

    @property
    def electronic_energy(self):
        return self.program.electronic_energy(self.output)

    @property
    def nuclear_repulsion_energy(self):
        return self.program.nuclear_repulsion_energy(self.output)

    @property
    def num_electrons(self):
        return self.program.num_electrons(self.output)

    @property
    def num_orbitals(self):
        return self.program.num_orbitals(self.output)

    @property
    def num_basis_functions(self):
        return self.program.num_basis_functions(self.output)

    @property
    def mo_energies(self):
        return self.program.mo_energies(self.output)

    @property
    def final_geometry(self):
        return self.program.final_geometry(self.output)

    @property
    def excitation_energies(self):
        return self.program.excitation_energies(self.output)

    @property
    def oscillator_strengths(self):
        return self.program.oscillator_strengths(self.output)

    @property
    def two_photon_strengths(self):
        return self.program.two_photon_strengths(self.output)

    @property
    def two_photon_cross_sections(self):
        return self.program.two_photon_cross_sections(self.output)

    @property
    def orbital_coefficients(self):
        return self.program.orbital_coefficients(self, self.output)

    @property
    def natural_occupations(self):
        return self.program.natural_occupations(self.output)

    def trim_natural_occupations(self, strong_threshold: float = 1.995, weak_threshold: float = 0.002) -> str:
        """Print relevant natural occupations in a nice format.

        Args:
          strong_threshold: Strongly occupied natural orbitals with occupations above this threshold will not be printed.
          weak_threshold: Weakly occupied natural orbitals with occupations below this threshold will not be printed.

        Returns
          String to be printed or saved to file.
        """
        if self._nat_occ_analyser is None:
            self._nat_occ_analyser = NaturalOccupationAnalyser(self.natural_occupations)
        self._nat_occ_analyser.strong_threshold = strong_threshold
        self._nat_occ_analyser.weak_threshold = weak_threshold
        return self._nat_occ_analyser.trimmed_natural_occ()

    def scan_occupations(self,
                         max_orbitals: int = 14,
                         strong_threshold: float = 1.995,
                         weak_threshold: float = 0.002) -> str:
        """Determine thresholds for picking active space.
        Will not print orbitals outside the bounds of "strong_threshold" and "weak_threshold".

        Args:
          max_orbitals: Maximum number of strongly occupied and weakly occupied orbitals to print.
          strong_threshold: Strongly occupied natural orbitals with occupations above this threshold will not be printed.
          weak_threshold: Weakly occupied natural orbitals with occupations below this threshold will not be printed.

        Returns
          String to be printed or saved to file.
        """
        if self._nat_occ_analyser is None:
            self._nat_occ_analyser = NaturalOccupationAnalyser(self.natural_occupations)
        self._nat_occ_analyser.strong_threshold = strong_threshold
        self._nat_occ_analyser.weak_threshold = weak_threshold
        return self._nat_occ_analyser.scan_occupations()

    def pick_cas_by_thresholds(
            self, max_strong_occupation: float,
            min_weak_occupation: float) -> Tuple[int, Union[int, List[int]], Union[int, List[int]]]:
        """Get parameters needed for a CAS calculation based on simple thresholds.

        Args:
          max_strong_occupation: Maximum occupation number of strongly occupied natural orbitals to be included in CAS.
          min_weak_occupation: Minimum occupation number of weakly occupied natural orbitals to be included in CAS.

        Returns:
          Number active electrons, CAS, number inactive orbitals.
        """
        if self._nat_occ_analyser is None:
            self._nat_occ_analyser = NaturalOccupationAnalyser(self.natural_occupations)
        return self._nat_occ_analyser.pick_cas_by_thresholds(max_strong_occupation, min_weak_occupation)

    @property
    def hyperfine_coupling(self):
        return self.program.hyperfine_coupling(self.output)

    def convolute_opa(self,
                      frequencies: np.ndarray,
                      broadening_function: Union[str, Callable[[np.ndarray, float, float], np.ndarray]] = 'gaussian',
                      broadening_factor: float = 0.4):
        """Convolute one-photon absorption.

        Args:
          frequencies: Frequency windows for the spectrum given in eV.
          broadening_function: Broadening function.
          broadening_factor: Broadening factor given in eV.

        Returns:
          Absorptivity in units of L / ( mol * cm ).
        """
        if isinstance(broadening_function, str):
            if broadening_function.lower() == 'gaussian':
                broadening_function = gaussian
            elif broadening_function.lower() == 'lorentzian':
                broadening_function = lorentzian
            else:
                raise ValueError(f'The given name of broadening function; {broadening_function}; is not valid.')
        excitation_energies = self.excitation_energies
        oscillator_strengths = self.oscillator_strengths
        return convolute_one_photon_absorption(excitation_energies,
                                               oscillator_strengths,
                                               frequencies,
                                               broadening_function=broadening_function,
                                               broadening_factor=broadening_factor)

    def convolute_tpa(self,
                      frequencies: np.ndarray,
                      broadening_function: Union[str, Callable[[np.ndarray, float, float], np.ndarray]] = 'gaussian',
                      broadening_factor: float = 0.1):
        """Convolute one-photon absorption.

        Args:
          frequencies: Frequency windows for the spectrum given in eV.
          broadening_function: Broadening function.
          broadening_factor: Broadening factor given in eV.

        Returns:
          Convoluted spectrum in units of GM.
        """
        if isinstance(broadening_function, str):
            if broadening_function.lower() == 'gaussian':
                broadening_function = gaussian
            elif broadening_function.lower() == 'lorentzian':
                broadening_function = lorentzian
            else:
                raise ValueError(f'The given name of broadening function; {broadening_function}; is not valid.')
        excitation_energies = self.excitation_energies
        two_photon_strengths = self.two_photon_strengths
        return convolute_two_photon_absorption(excitation_energies,
                                               two_photon_strengths,
                                               frequencies,
                                               broadening_function=broadening_function,
                                               broadening_factor=broadening_factor)
