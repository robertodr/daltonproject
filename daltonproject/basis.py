from typing import Dict, Union


class Basis:
    """Specify the AO basis.
    """
    def __init__(self, basis: Union[Dict[str, str], str], ri=None, admm=None) -> None:
        """Initialize Basis class.

        Args:
          basis: Basis set for molecule.
                 Can be a string, if same basis set should be applied to all atoms.
                 Can be a dict of the type {atom_name: basis} if different basis sets are to be used for different atoms.
          ri: Basis set for RI.
              Follows same format as basis.
          admm: Basis set for ADMM.
                Follows same format as basis.
        """
        self.basis = self.validate_basis(basis)
        if ri is not None:
            self.ri = self.validate_basis(ri)
        if admm is not None:
            self.admm = self.validate_basis(admm)

    def validate_basis(self, basis: Union[Dict[str, str], str]) -> Union[Dict[str, str], str]:
        """Validate basis set.

        Args:
          basis: Basis object (str or dict)

        Returns:
          Basis object (str or dict)
        """
        if not isinstance(basis, str) and not isinstance(basis, dict):
            raise TypeError(f'Unsupported basis set type: {type(basis)}')
        if isinstance(basis, dict):
            for key, value in basis.items():
                if not isinstance(key, str):
                    raise TypeError(f'Invalid atom label type: {type(key)}')
                if not isinstance(value, str):
                    raise TypeError(f'Unknown basis type: {type(value)}')
        return basis
