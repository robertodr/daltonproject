from typing import Callable

import numpy as np
from qcelemental import constants


def lorentzian(x: np.ndarray, x0: float, broadening_factor: float) -> np.ndarray:
    """Normalized Lorentzian broadening function.

    Args:
      x: Where to evaluate the Lorentzian.
      x0: Center of Lorentzian.
      broadening_factor: Width of Lorentzian.

    Returns:
      A Lorentzian.
    """
    return broadening_factor / (np.pi * (broadening_factor**2 + (x - x0)**2))


def gaussian(x: np.ndarray, x0: float, broadening_factor: float) -> np.ndarray:
    """Normalized Gaussian broadening function.

    Args:
      x: Where to evaluate the Gaussian.
      x0: Center of Gaussian.
      broadening_factor: Half width at half maximum.

    Returns:
      A Gaussian.
    """
    normalize_factor = np.sqrt((np.log(2) / broadening_factor**2) / np.pi)
    return normalize_factor * np.exp(-np.log(2) * ((x - x0) / broadening_factor)**2)


def convolute_one_photon_absorption(
        excitation_energies: np.ndarray,
        oscillator_strengths: np.ndarray,
        frequencies: np.ndarray,
        broadening_function: Callable[[np.ndarray, float, float], np.ndarray] = gaussian,
        broadening_factor: float = 0.4,
) -> np.ndarray:
    """Convolute one-photon absorption.
    The convolution is based on the equations of:
    A. Rizzo, S. Coriani, and K. Ruud, in Computational Strategies for Spectroscopy.
    From Small Molecules to Nano Systems,
    edited by V. Barone (John Wiley and Sons, 2012) Chap. 2, pp. 77–135

    Args:
      excitation_energies: Excitation energies.
      oscillator_strengths: Oscillator strengths.
      frequencies: Frequency windows for the spectrum given in eV.
      broadening_function: Broadening function.
      broadening_factor: Broadening factor given in eV.

    Returns:
      Absorptivity in units of L / ( mol * cm ).
    """
    result = np.zeros(frequencies.shape[0])
    broadening_factor = broadening_factor / constants.hartree2ev
    frequencies = frequencies / constants.hartree2ev
    excitation_energies = excitation_energies / constants.hartree2ev

    # 4.361314937e19 is derived from simplifying the constants:
    # e**2 * pi * N_A / ( 2 * ln(10) * epsilon_0 * m_e * c ) into units of L / ( mol * cm * s )
    # 2 * pi * 6.57968e15 is to convert the broadening factor from au to s**-1 (angular frequency).
    # Combining the two numbers above gives 6628.460563431657 / ( 2 * pi ).
    prefactor = 6628.460563431657 / (2 * np.pi)

    for excitation_energy, oscillator_strength in zip(excitation_energies, oscillator_strengths):
        result += (prefactor * oscillator_strength * frequencies / excitation_energy *
                   broadening_function(frequencies, excitation_energy, broadening_factor))
    return result


def convolute_two_photon_absorption(
        excitation_energies: np.ndarray,
        two_photon_strengths: np.ndarray,
        frequencies: np.ndarray,
        broadening_function: Callable[[np.ndarray, float, float], np.ndarray] = lorentzian,
        broadening_factor: float = 0.1,
) -> np.ndarray:
    """Convolute two-photon absorption.

    Args:
      excitation_energies: Excitation energies.
      two_photon_strengths: Two-photon oscillator strengths.
      frequencies: Frequency windows for the spectrum given in eV.
      broadening_function: Broadening function.
      broadening_factor: Broadening factor given in eV.

    Returns:
      Convoluted spectrum in units of GM.
    """
    result = np.zeros(frequencies.shape[0])
    # eV to au.
    frequencies = frequencies / constants.hartree2ev
    excitation_energies = excitation_energies / constants.hartree2ev
    broadening_factor = broadening_factor / constants.hartree2ev
    au2gm = constants.h / (2 * np.pi) / constants.hartree2J * constants.bohr2cm**4 * 1e50
    prefactor = 8 * (np.pi**3) * au2gm / (constants.c_au**2)

    for excitation_energy, two_photon_strength in zip(excitation_energies, two_photon_strengths):
        result += (prefactor * two_photon_strength * frequencies**2 *
                   broadening_function(2 * frequencies, excitation_energy, broadening_factor))
    return result
