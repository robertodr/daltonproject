from typing import Any, Dict, List, Union

import numpy as np


class QCMethod:
    """Specify the QC method including all associated settings."""
    def __init__(self, qc_method: str, xc_functional: str = None, exchange: str = None, coulomb: str = None) -> None:
        """Initializer of QCMethod class.

        Args:
          qc_method: Name of the quantum chemical method.
          xc_functional: Name of the exchange-correlation functional.
          exchange: Name of the approximation to use for the exchange contribution.
          coulomb: Name of the approximation to use for the Coulomb contribution.
        """
        self.settings: Dict[str, Any] = {}
        self.qc_method(qc_method=qc_method)
        if xc_functional is not None:
            self.xc_functional(xc_functional)
        if coulomb is not None:
            self.coulomb(coulomb)
        if exchange is not None:
            self.exchange(exchange)

    def qc_method(self, qc_method: str) -> None:
        """Quantum Chemical Method
        Specify the quantum chemical method to use.

        Args:
          qc_method: Name of the quantum chemical method.
        """
        if not isinstance(qc_method, str):
            raise TypeError('QC method must be given as a string.')
        self.settings.update({'qc_method': qc_method.upper()})

    def xc_functional(self, xc_functional: str) -> None:
        """Exchange-Correlation functional
        Specify the exchange-correlation functional to use with DFT methods.

        Args:
          xc_functional: Name of the exchange-correlation functional.
        """
        if not isinstance(xc_functional, str):
            raise TypeError('XC functional must be given as a string.')
        if self.settings['qc_method'] not in ['DFT', 'HFSRDFT', 'MP2SRDFT', 'CASSRDFT']:
            raise TypeError('Specifying XC functional is only valid together with DFT methods.')
        self.settings.update({'xc_functional': xc_functional.upper()})

    def scf_threshold(self, threshold: float) -> None:
        """SCF threshold
        Specify the convergence threshold for SCF calculations.

        Args:
          threshold: Threshold for SCF convergence in Hartree.
        """
        if not isinstance(threshold, float):
            raise TypeError('SCF threshold must be given as a float.')
        self.settings.update({'scf_threshold': threshold})

    def coulomb(self, coulomb: str) -> None:
        """Coulomb

        Args:
          coulomb: Name of the approximation to use for the Coulomb contribution.
        """
        self.settings.update({'coulomb': coulomb})

    def exchange(self, exchange: str) -> None:
        """Exchange

        Args:
          exchange: Name of the approximation to use for the exchange contribution.
        """
        self.settings.update({'exchange': exchange})

    def complete_active_space(
            self,
            num_active_electrons: int,
            num_cas_orbitals: Union[List[int], int],
            num_inactive_orbitals: Union[List[int], int],
    ) -> None:
        """Complete active space

        Args:
          num_active_electrons: Number of active electrons.
                                   // Maybe this should be infered just from the num_inactive_orbitals?
          num_cas_orbitals: List of number of orbitals in the complete active space.
          num_inactive_orbitals: List of number of inactive (double occipied) orbitals.
        """
        if not isinstance(num_active_electrons, int):
            raise TypeError('num_active_electrons must be given as an integer.')
        if isinstance(num_inactive_orbitals, int):
            # In case of NoSymmetry the function can take in the number of inactive orbitals as an integer.
            num_inactive_orbitals = [num_inactive_orbitals]
        if isinstance(num_cas_orbitals, int):
            # In case of NoSymmetry the function can take in the number of cas orbitals as an integer.
            num_cas_orbitals = [num_cas_orbitals]
        for num_orbitals in num_inactive_orbitals:
            if not isinstance(num_orbitals, int):
                raise TypeError('num_inactive_orbitals must only be integers.')
        for num_orbitals in num_cas_orbitals:
            if not isinstance(num_orbitals, int):
                raise TypeError('num_cas_orbitals must only be integers.')
        if len(num_inactive_orbitals) != len(num_cas_orbitals):
            raise ValueError('Dimension of num_inactive_orbitals and num_cas_orbitals must be the same.')
        if num_active_electrons > np.sum(num_cas_orbitals) * 2:
            raise ValueError('Too many active electrons for the number of CAS orbitals.')
        if self.settings['qc_method'] not in ['CASSCF', 'HFSRDFT', 'CASSRDFT']:  #
            raise TypeError('Specifying CAS is only valid for range multi-configurational methods.')
        self.settings.update({'num_inactive_orbitals': num_inactive_orbitals})
        self.settings.update({'num_cas_orbitals': num_cas_orbitals})
        self.settings.update({'num_active_electrons': num_active_electrons})

    def range_separation_parameter(self, mu: float) -> None:
        """Method for setting the value of the range separation parameter.
        The range separation parameter is used in srDFT.

        Args:
          mu: Range separation parameter in the unit of a.u.^-1.
        """
        if isinstance(mu, int):
            mu = float(mu)
        if not isinstance(mu, float):
            raise TypeError('mu, the range separation parameter, must be given as a float.')
        if self.settings['qc_method'] not in [
                'DFT',
                'HFSRDFT',
                'MP2SRDFT',
                'CASSRDFT',
        ]:
            raise TypeError('Specifying range separation parameter is only valid for range separated methods.')
        self.settings.update({'range_separation_parameter': mu})

    def input_orbital_coefficients(self, orbitals: Union[np.ndarray, Dict[int, np.ndarray]]) -> None:
        """Method for specified start orbitals.

        Args:
          orbitals: Orbital coefficients.
        """
        if not isinstance(orbitals, np.ndarray) and not isinstance(orbitals, dict):
            raise TypeError('Orbitals must be an array or multiple arrays in a dictionary.')
        self.settings.update({'orbitals': orbitals})
