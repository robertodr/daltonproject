import warnings
from typing import List

import numpy as np
from qcelemental import periodictable

from .symmetry import SymmetryAnalysis


class Molecule:
    """Molecule class.
    """
    def __init__(self, atoms: str = None, charge: int = 0, xyz: str = None, symmetry: bool = False) -> None:
        """Initialize Molecule class.

        Args:
          atoms: Atoms specified with elements and coordinates (and optionally labels).
          charge: Total charge of molecule.
          xyz: Path to xyz file containing atoms and coordinates (and optionally labels in a 5th column).
        """
        if not isinstance(charge, int):
            raise TypeError('Charge must be an integer.')
        self.charge = charge
        self.elements: List[str] = []
        self.labels: List[str] = []
        self._coordinates = np.ndarray([])
        self.symmetry_threshold = 1e-3
        self.point_group = 'C1'
        if atoms is not None and xyz is not None:
            raise SyntaxError('Specify either atoms or xyz.')
        if xyz is not None:
            self.xyz_reader(xyz)
        if atoms is not None:
            self.atoms(atoms)
        if symmetry:
            self.analyse_symmetry()

    def atoms(self, atoms: str) -> None:
        """Specify atoms.

        Args:
          atoms: Atoms specified with elements and coordinates (and optionally labels).
        """
        if not isinstance(atoms, str):
            raise TypeError("Atoms are given as a string using either '\\n' or ';' to separate atoms.")
        if ';' in atoms:
            lines = atoms.split(';')
        elif '\n' in atoms:
            lines = atoms.split('\n')
        elif not 4 <= len(atoms.split()) <= 5:
            raise ValueError(f'Incompatible input: {atoms}')
        else:
            # Assume it is a single atom.
            lines = [atoms]

        elements = []
        labels = []
        coordinates = []
        for line in lines:
            atom = line.split()
            element = atom[0].strip().title()
            if element not in periodictable.E:
                raise ValueError(f'Unknown element: {element}.')
            elements.append(element)
            coordinates.append(np.array([float(value) for value in atom[1:4]]))
            if len(atom) == 5:
                labels.append(atom[4].strip())
            else:
                labels.append(atom[0].strip())
        self.elements = elements
        self.labels = labels
        self.coordinates = np.array(coordinates)

    def xyz_reader(self, filename: str) -> None:
        """Read xyz file.

        Args:
          filename: Name of xyz file containing atoms and coordinates.
        """
        with open(f'{filename}', 'r') as xyz_file:
            lines = xyz_file.read().split('\n')
        try:
            num_atoms = int(lines[0])
        except ValueError:
            raise TypeError('First line in xyz file cannot be converted to an integer.')
        # Remove trailing empty lines.
        while lines[-1].strip() == '':
            del lines[-1]
        if len(lines[2:]) > num_atoms:
            warnings.warn(
                f'Only {num_atoms} atoms were declared but xyz file apparently contains {len(lines[2:])} lines. Only {num_atoms} will be read.'
            )
        if len(lines[2:]) < num_atoms:
            raise ValueError(f'{num_atoms} atoms were declared, but file only contains {len(lines[2:])} lines.')
        atom_lines = ''
        # Skip the declaration and comment line.
        for line in lines[2:num_atoms + 2]:
            coordinate_line = line.split()
            # We allow lines to be longer than 4 and 5 values in case the user adds comments etc.
            if len(coordinate_line) < 4:
                raise ValueError(f'Wrong number of values in line:\n{line}')
            if coordinate_line[0].strip().title() not in periodictable.E:
                raise ValueError(f'Unknown element in line:\n{line}.')
            try:
                [float(value) for value in coordinate_line[1:4]]
            except ValueError:
                raise TypeError(f'Cannot read coordinates from line:\n{line}')
            # Add five values in case there are atom labels but remove the rest (if present).
            atom_lines += ' '.join(coordinate_line[0:5]) + '\n'
        # The -1 is to remove the trailing "\n".
        self.atoms(atom_lines[:-1])

    @property
    def coordinates(self):
        return self._coordinates

    @coordinates.setter
    def coordinates(self, coordinates):
        if not len(coordinates) == self.num_atoms:
            raise Exception('Number of coordinates does not correspond to number of atoms in molecule.')
        if isinstance(coordinates, np.ndarray):
            self._coordinates = coordinates
        elif isinstance(coordinates, list):
            self._coordinates = np.array(coordinates)

    @property
    def num_atoms(self):
        return len(self.elements)

    def analyse_symmetry(self) -> None:
        """Analyse the molecular symmetry. Note that this translates the molecules center of mass to the origin.
        """
        self.symmetry = SymmetryAnalysis(self.elements,
                                         self.coordinates,
                                         symmetry_threshold=self.symmetry_threshold,
                                         labels=self.labels)
        self.coordinates, self.point_group = self.symmetry.find_symmetry()
