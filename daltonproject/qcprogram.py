from typing import Any, Dict, Union

from .dalton_ifc import DaltonIFC
from .lsdalton_ifc import LSDaltonIFC

programs: Dict[str, Any] = {'dalton': DaltonIFC, 'lsdalton': LSDaltonIFC}


def QCProgram(program: str) -> Union[DaltonIFC, LSDaltonIFC]:
    """Quantum Chemical Program.

    Args:
      program: Name of quantum chemical program.

    Returns:
      A class object associated with the specified quantum chemical program.
    """
    if not isinstance(program, str):
        raise TypeError('Program must be given as a string')
    if program.lower() not in programs.keys():
        raise ValueError(f'Program {program} is not supported')
    return programs[program.lower()]()
