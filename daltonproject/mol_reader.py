from typing import Dict, Tuple

import numpy as np
from qcelemental import periodictable

from .basis import Basis
from .molecule import Molecule


def mol_reader(filename: str) -> Tuple[Molecule, Basis]:
    """Read Dalton molecule input file.

    Args:
      filename: Name of Dalton molecule input file containing atoms and coordinates.

    Returns:
      Molecule object and Basis object.
    """
    with open(f'{filename}', 'r') as mol_file:
        lines = mol_file.read().split('\n')
    with_generator = False
    with_atombasis = False
    if 'ATOMBASIS' in lines[0]:
        with_atombasis = True
        basis_set: Dict[str, str] = {}
    else:
        basis_set = {'without_atombasis': lines[1]}
    if with_atombasis:
        line_list = lines[3].replace(' ', '=').split('=')
    else:
        line_list = lines[4].replace(' ', '=').split('=')
    # Finding the charge of the molecule.
    if 'Charge' in line_list:
        Charge = int(float(line_list[line_list.index('Charge') + 1]))
    else:
        Charge = 0
    if 'Generators' in line_list:
        with_generator = True
        num_generators = int(float(line_list[line_list.index('Generators') + 1]))
        X1, Y1, Z1, X2, Y2, Z2, X3, Y3, Z3 = 1, 1, 1, 1, 1, 1, 1, 1, 1
        if num_generators >= 1:
            generator_1 = line_list[line_list.index('Generators') + 2]
            if 'X' in generator_1:
                X1 = -1
            if 'Y' in generator_1:
                Y1 = -1
            if 'Z' in generator_1:
                Z1 = -1
        if num_generators >= 2:
            generator_2 = line_list[line_list.index('Generators') + 3]
            if 'X' in generator_2:
                X2 = -1
            if 'Y' in generator_2:
                Y2 = -1
            if 'Z' in generator_2:
                Z2 = -1
        if num_generators >= 3:
            generator_3 = line_list[line_list.index('Generators') + 4]
            if 'X' in generator_3:
                X3 = -1
            if 'Y' in generator_3:
                Y3 = -1
            if 'Z' in generator_3:
                Z3 = -1
    # ----------------------------------------------------------
    # Read coordinates and labels (and basis set if ATOMBASIS).
    # ----------------------------------------------------------
    atom_counter = 0
    num_atoms = 0
    current_basis = ''
    atom_lines = ''
    if with_atombasis:
        start_idx = 3
    else:
        start_idx = 4
    if not with_generator:
        for line in lines[start_idx:]:
            if 'Atoms' in line:
                line_list = line.replace(' ', '=').split('=')
                # Find which element it is.
                current_element = periodictable.to_symbol(int(float(line_list[line_list.index('Charge') + 1])))
                if with_atombasis:
                    # Determine basis set for coming labels.
                    current_basis = line_list[line_list.index('Basis') + 1]
                # Reset atom_counter.
                atom_counter = 0
                num_atoms = int(float(line_list[line_list.index('Atoms') + 1]))
            elif atom_counter < num_atoms:
                atom_counter += 1
                coordinate = np.array([float(line.split()[1]), float(line.split()[2]), float(line.split()[3])])
                label = line.split()[0]
                atom_lines += f'{current_element} {coordinate[0]} {coordinate[1]} {coordinate[2]} {label};'
                if with_atombasis:
                    basis_set[label] = current_basis
    else:
        # Handle reading coordinates from generator.
        for line in lines[start_idx:]:
            if 'Atoms' in line:
                line_list = line.replace(' ', '=').split('=')
                # Find which element it is.
                current_element = periodictable.to_symbol(int(float(line_list[line_list.index('Charge') + 1])))
                if with_atombasis:
                    # Determine basis set for coming labels.
                    current_basis = line_list[line_list.index('Basis') + 1]
                # Reset atom_counter.
                atom_counter = 0
                num_atoms = int(float(line_list[line_list.index('Atoms') + 1]))
            elif atom_counter < num_atoms:
                atom_counter += 1
                coordinate = np.array([float(line.split()[1]), float(line.split()[2]), float(line.split()[3])])
                label = line.split()[0]
                atom_lines += f'{current_element} {coordinate[0]} {coordinate[1]} {coordinate[2]} {label};'
                if with_atombasis:
                    basis_set[label] = current_basis
                found_coordinates = [coordinate.tolist()]
                next_coordinate = np.array([X1 * coordinate[0], Y1 * coordinate[1], Z1 * coordinate[2]])
                # The if statement is to ensure that the generator wont place an atom on top of another atom.
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]} {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
                next_coordinate = np.array([X2 * coordinate[0], Y2 * coordinate[1], Z2 * coordinate[2]])
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]} {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
                next_coordinate = np.array([X3 * coordinate[0], Y3 * coordinate[1], Z3 * coordinate[2]])
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]} {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
                next_coordinate = np.array(
                    [X1 * X2 * coordinate[0], Y1 * Y2 * coordinate[1], Z1 * Z2 * coordinate[2]])
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]} {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
                next_coordinate = np.array(
                    [X1 * X3 * coordinate[0], Y1 * Y3 * coordinate[1], Z1 * Z3 * coordinate[2]])
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]} {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
                next_coordinate = np.array(
                    [X2 * X3 * coordinate[0], Y2 * Y3 * coordinate[1], Z2 * Z3 * coordinate[2]])
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]} {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
                next_coordinate = np.array([
                    X1 * X2 * X3 * coordinate[0],
                    Y1 * Y2 * Y3 * coordinate[1],
                    Z1 * Z2 * Z3 * coordinate[2],
                ])
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]} {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
    # The -1 is to remove the trailing ";".
    molecule = Molecule(atoms=atom_lines[:-1], charge=Charge)
    if with_atombasis:
        basis = Basis(basis=basis_set)
    else:
        basis = Basis(basis=basis_set['without_atombasis'])
    return molecule, basis
