Installation
============
Dalton Project can be acquired by downloading the latest build from GitLab::

    $ git clone https://gitlab.com/DaltonProject/daltonproject.git

All required Python packages can be installed using **pip**::

    $ pip install -r requirements.txt


To install Dalton Project::

    $ python setup.py install

Alternatively, add it to **PYTHONPATH**::

    $ export PYTHONPATH=$PYTHONPATH:{path_to_daltonporject_folder}

An example of this could be::

    $ export PYTHONPATH=$PYTHONPATH:/home/smith/daltonproject/

Dalton Project will in general expect the programs executables to be in **PATH**.
The path to the executables can be added to **PATH** by doing the following command::

    $ export PATH="{path_to_folder_with_executable}:$PATH"

An example of this could be for the Dalton program, where the program has been compiled in the **build** directory::

    $ export PATH="/home/smith/dalton/build:$PATH"

Note that the export command can be added to **~/.bashrc** to automatically be run whenever a new terminal is opened.

Running tests
-------------
The tests can be run by executing the following command within the Dalton Project folder::

    $ pytest

Dalton
------
The `Dalton program <https://daltonprogram.org/>`_ can be downloaded from `here <https://daltonprogram.org/download/>`_.
The installation guide for the program can be found `here <https://dalton-installation.readthedocs.io/en/latest/>`_.


LSDalton
--------
The `Dalton program <https://daltonprogram.org/>`_ can be downloaded from `here <https://daltonprogram.org/download/>`_.
The installation guide for the program can be found `here <https://dalton-installation.readthedocs.io/en/latest/>`_.
