Development guide
=================

Here is a brief outline for the development goal in terms of code, readability, and maintainability.

Code Style
----------

The code style is enforced using `pre-commit <https://pre-commit.com/>` hooks that are run in the GitLab CI pipeline.
It will check and format Python source files using the following code-style checkers and formatters

- `Flake8 <https://github.com/PyCQA/flake8>`_
- `YAPF <https://github.com/google/yapf>`_
- `isort <https://github.com/timothycrosley/isort>`_

In addition, it will check and fix other file types for trailing whitespace and more.
These code-style checkers and formatters are used to make the code-base look uniform and check that it is PEP8 compliant.
Note that the line-length limit is set to 99 characters even though PEP8 recommends 79.

The pre-commit hooks are set up locally by running the following two commands in the root of the Dalton Project directory::

    $ pip install pre-commit
    $ pre-commit install

During a **git commit**, if any pre-commit hook fails, mostly you will simply need to **git add** the affected files and **git commit** again, because most tools will automatically reformat the files.
Staged files can also be checked before committing by running::

    $ pre-commit run

Again, if a step fails the tools will in most cases also reformat the files that caused the failure.
You will therefore need to stage those files again (**git add**) and they are then ready to **git commit**.

Continuous Integration and Code Coverage
----------------------------------------

The Dalton Project uses continuous integration and code coverage.

Documentation
-------------

The documentation is compiled using `sphinx <https://github.com/sphinx-doc/sphinx>`_.
The documentation is automatically hosted using `Read the Docs <https://readthedocs.org/>`_.
Note that **autodoc** is enabled for `Google-Style DocStrings <https://google.github.io/styleguide/pyguide.html>`_.

The documentation can be generated locally by running the following command::

    $ sphinx-build docs local_docs

This requires `sphinx rtd theme <https://github.com/readthedocs/sphinx_rtd_theme>`_.
This theme is available through **pip**::

    $ pip install sphinx_rtd_theme

and **conda**::

    $ conda install -c anaconda sphinx_rtd_theme
